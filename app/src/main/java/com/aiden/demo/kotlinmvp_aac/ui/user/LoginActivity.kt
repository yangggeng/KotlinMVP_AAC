package com.aiden.demo.kotlinmvp_aac.ui.user

import com.aiden.app.common.base.PresenterActivity
import com.aiden.app.common.extension.onClick
import com.aiden.app.common.extension.sToast
import com.aiden.demo.kotlinmvp_aac.R
import com.aiden.demo.kotlinmvp_aac.presenter.LoginPresenter
import com.aiden.demo.kotlinmvp_aac.view.ILoginView
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : PresenterActivity<LoginPresenter>(), ILoginView {

    override fun onCreate() {
        setContentView(R.layout.activity_login)
        setTitle(true, "登录")
    }

    override fun initListener() {
        btn_login.onClick {
            if (et_username.text.trim().isEmpty()) {
                sToast("请输入用户名")
                return@onClick
            }
            if (et_password.text.trim().isEmpty()) {
                sToast("请输入密码")
                return@onClick
            }
            mPresenter.login(et_username.text.trim().toString(), et_password.text.trim().toString())
        }
    }

    override fun loadData() {
    }

    override fun presenter(): LoginPresenter {
        return LoginPresenter()
    }

    override fun view() {
        mPresenter.loadView(this)
    }

    override fun loginSuccess(id: String) {
        sToast("登录成功")
    }

    override fun loginFailed(error: String) {
        sToast(error)
    }
}