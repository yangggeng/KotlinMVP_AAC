package com.aiden.demo.kotlinmvp_aac

import com.aiden.app.common.base.BaseApplication
import com.aiden.demo.kotlinmvp_aac.constants.Url

class WanApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        initNetwork(Url.BaseUrl)
    }
}