package com.aiden.demo.kotlinmvp_aac.presenter

import android.arch.lifecycle.Observer
import com.aiden.app.common.base.BasePresenter
import com.aiden.demo.kotlinmvp_aac.repository.UserRemoteRepository
import com.aiden.demo.kotlinmvp_aac.ui.user.LoginActivity
import com.aiden.demo.kotlinmvp_aac.viewmodel.LoginViewModel

class LoginPresenter : BasePresenter<LoginActivity>() {

    private val loginViewModel = LoginViewModel(UserRemoteRepository())

    override fun onCreate() {
        super.onCreate()
        loginViewModel.getData().observe(view, Observer {
            it?.apply {
                if (isSuccessful) {
                    view.loginSuccess(data!!)
                } else {
                    view.loginFailed(errorMsg)
                }
            }
        })
    }

    fun login(username: String, password: String) {
        loginViewModel.login(username, password)
    }
}