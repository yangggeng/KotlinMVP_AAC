package com.aiden.demo.kotlinmvp_aac

import android.os.Bundle
import com.aiden.app.common.base.BaseActivity
import com.aiden.app.common.extension.onClick
import com.aiden.demo.kotlinmvp_aac.ui.user.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_login.onClick {
            startActivity<LoginActivity>()
        }
    }
}
