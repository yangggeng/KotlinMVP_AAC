package com.aiden.demo.kotlinmvp_aac.viewmodel

import com.aiden.app.common.base.BaseViewModel
import com.aiden.app.common.http.BaseResponse
import com.aiden.demo.kotlinmvp_aac.repository.IUserRepository

class LoginViewModel(private val userRepository: IUserRepository) : BaseViewModel<BaseResponse<String?>>() {

    fun login(username: String, password: String) {
        userRepository.login(liveData, username, password)
    }
}