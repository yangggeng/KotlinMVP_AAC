package com.aiden.demo.kotlinmvp_aac.repository

import android.arch.lifecycle.MutableLiveData
import com.aiden.app.common.http.BaseResponse

/**
 *
 *
 * @author yanggeng
 * @date 2019/7/31 16:16
 */
interface IUserRepository {

    fun login(liveData: MutableLiveData<BaseResponse<String?>>, username: String, password: String)
}