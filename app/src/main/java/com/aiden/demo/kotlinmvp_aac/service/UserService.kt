package com.aiden.demo.kotlinmvp_aac.service

import android.arch.lifecycle.LiveData
import com.aiden.app.common.http.BaseResponse
import com.aiden.demo.kotlinmvp_aac.constants.Url
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UserService {

    @FormUrlEncoded
    @POST(Url.User_Login)
    fun login(@Field("username") username: String, @Field("password") password: String): LiveData<BaseResponse<String?>>

    @FormUrlEncoded
    @POST(Url.User_Register)
    fun register(@Field("username") username: String, @Field("password") password: String, @Field("repassword") repassword: String): LiveData<BaseResponse<String?>>
}