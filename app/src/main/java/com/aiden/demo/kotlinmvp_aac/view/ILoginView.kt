package com.aiden.demo.kotlinmvp_aac.view

import com.aiden.app.common.base.IBaseView

interface ILoginView : IBaseView {

    fun loginSuccess(id: String)
    fun loginFailed(error: String)
}