package com.aiden.demo.kotlinmvp_aac.constants

object Url {
    const val BaseUrl = "https://www.wanandroid.com"

    const val User_Login = "/user/login"
    const val User_Register = "/user/register"
    const val User_Logout = "/user/logout/json"
}
