package com.aiden.demo.kotlinmvp_aac.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.aiden.app.common.http.BaseResponse
import com.aiden.app.common.http.NetWork
import com.aiden.demo.kotlinmvp_aac.service.UserService

class UserRemoteRepository : IUserRepository {

    private val userService = NetWork.INSTANCE.getService(UserService::class.java)

    override fun login(liveData: MutableLiveData<BaseResponse<String?>>, username: String, password: String) {
        val temp = userService.login(username, password)
        temp.observeForever(object : Observer<BaseResponse<String?>> {
            override fun onChanged(t: BaseResponse<String?>?) {
                liveData.value = t
                temp.removeObserver(this)
            }
        })
    }
}