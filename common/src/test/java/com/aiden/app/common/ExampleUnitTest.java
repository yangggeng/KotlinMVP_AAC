package com.aiden.app.common;

import com.aiden.app.common.util.HexUtil;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testHex() {
        String srcStr = "待转换字符串";
        String encodeStr = HexUtil.INSTANCE.encodeHexStr(srcStr.getBytes());
        String decodeStr = new String(HexUtil.INSTANCE.decodeHex(encodeStr.toCharArray()));
        System.out.println("转换前：" + srcStr);
        System.out.println("转换后：" + encodeStr);
        System.out.println("还原后：" + decodeStr);
    }
}