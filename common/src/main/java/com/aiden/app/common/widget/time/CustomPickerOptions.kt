package com.aiden.app.common.widget.time

import com.bigkoo.pickerview.configure.PickerOptions

/**
 * 增加扩展参数
 *
 * @author yanggeng
 * @date 2019/5/21 11:28
 */
class CustomPickerOptions(buildType: Int): PickerOptions(buildType) {

    var extrasArray = mutableListOf<String>()

    var onTimeSelectWithExtraListener: OnTimeSelectListener ?= null
}