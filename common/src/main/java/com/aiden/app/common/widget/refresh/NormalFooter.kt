package com.aiden.app.common.widget.refresh

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.aiden.app.common.R
import com.aiden.app.common.extension.setVisible
import com.scwang.smartrefresh.layout.api.RefreshFooter
import com.scwang.smartrefresh.layout.api.RefreshKernel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.constant.RefreshState
import com.scwang.smartrefresh.layout.constant.SpinnerStyle

/**
 * 公共上拉加载Footer
 *
 * @author yanggeng
 * @date 2019/6/25 15:33
 */
class NormalFooter @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttrs: Int = 0) : FrameLayout(context, attrs, defStyleAttrs), RefreshFooter {

    private var ivRefresh: ImageView
    private var tvFailed: TextView
    private var animationDrawable: AnimationDrawable
    private var selfView: View

    init {
        View.inflate(context, R.layout.refresh_view, this)
        selfView = this
        ivRefresh = selfView.findViewById(R.id.iv_refresh)
        tvFailed = selfView.findViewById(R.id.tv_failed)
        ivRefresh.setImageResource(R.drawable.comm_refresh)
        animationDrawable = ivRefresh.drawable as AnimationDrawable
    }

    override fun getView(): View {
        return this
    }

    override fun getSpinnerStyle(): SpinnerStyle {
        return SpinnerStyle.Translate
    }

    override fun onStartAnimator(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {
        animationDrawable.start()
    }

    override fun onFinish(refreshLayout: RefreshLayout, success: Boolean): Int {
        if (animationDrawable.isRunning) {
            animationDrawable.stop()
        }
        tvFailed.setVisible(!success)
        tvFailed.setText(R.string.net_load_more_failed)
        ivRefresh.setVisible(success)
        return 200
    }

    override fun onStateChanged(refreshLayout: RefreshLayout, oldState: RefreshState, newState: RefreshState) {
        when (newState) {
            RefreshState.None, RefreshState.PullUpToLoad -> {
                animationDrawable.stop()
            }
            RefreshState.PullUpCanceled -> {
                animationDrawable.stop()
            }
            RefreshState.ReleaseToLoad -> {
                animationDrawable.start()
            }
            RefreshState.LoadReleased -> {
                animationDrawable.start()
            }
            RefreshState.Loading -> {
                animationDrawable.start()
            }
            RefreshState.LoadFinish -> {
                animationDrawable.stop()
            }
        }

    }

    override fun onInitialized(kernel: RefreshKernel, height: Int, maxDragHeight: Int) {
    }

    override fun onHorizontalDrag(percentX: Float, offsetX: Int, offsetMax: Int) {
    }

    override fun setNoMoreData(noMoreData: Boolean): Boolean {
        tvFailed.setVisible(noMoreData)
        if (noMoreData) {
            tvFailed.setText(R.string.net_no_more_data)
            ivRefresh.setVisible(false)
        }
        return true
    }

    override fun onReleased(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {

    }

    override fun setPrimaryColors(vararg colors: Int) {
    }

    override fun onMoving(isDragging: Boolean, percent: Float, offset: Int, height: Int, maxDragHeight: Int) {
    }

    override fun isSupportHorizontalDrag(): Boolean {
        return false
    }
}