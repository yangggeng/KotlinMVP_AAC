package com.aiden.app.common.entity

import android.support.annotation.DrawableRes

data class TabView(var text: String, @DrawableRes var icon: Int, var count: Int = 0, var desc: String = "")