package com.aiden.app.common.entity

import com.aiden.app.common.extension.*
import com.google.gson.Gson
import java.io.Serializable
import java.util.*

/**
 * 网络请求时间戳
 *
 * @author yanggeng
 * @date 2019/4/25 15:11
 */
data class TimeMillis(var seconds: String = "", var nanos: String = "0") : Serializable {

    override fun toString(): String {
        return Gson().toJson(this).replace("\\", "")
    }

    fun format2String(): String {
        return Date(seconds.toLong() * 1000L).formatYMDHMS()
    }
    fun format2StringDot(): String {
        return Date(seconds.toLong() * 1000L).formatYMD_Dot()
    }

    fun format2StringSimple(): String {
        return Date(seconds.toLong() * 1000L).formatYMD()
    }

    fun getTimeDateOnly(): TimeDateOnly {
        if(seconds.isNil()){
            return TimeDateOnly()
        }
        return Date(seconds.toLong() * 1000L).getTimeDateOnly()
    }


//    fun format2String(formatType: DateFormatType = DateFormatType.Detail_WithSec): String {
//        return DateUtils.long2String(seconds.toLong() * 1000L, formatType)
//    }
}