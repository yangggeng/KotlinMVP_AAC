package com.aiden.app.common.impl

import android.text.Editable
import android.text.TextWatcher

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/4/18
 * Email：ibelieve1210@163.com
 */
abstract class TextWatcherPro : TextWatcher {

//    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//    }

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }
}