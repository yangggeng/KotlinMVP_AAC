package com.aiden.app.common.util

import android.annotation.SuppressLint
import com.aiden.app.common.constant.DateFormatType
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    /**
     * 将时间戳转换成日期字符串
     */
    @SuppressLint("SimpleDateFormat")
    fun long2String(timestamp: Long, formatType: DateFormatType = DateFormatType.Simply_DateOnly): String {
        return SimpleDateFormat(formatType.fromContent, Locale.CHINA).format(Date(timestamp))
    }

    fun getHMS(millis: Long): Array<String> {
        val hours = millis / (3600000)
        val minutes = (millis - hours * 3600000) / 60000
        val seconds = (millis - hours * 3600000 - minutes * 60000) / 1000
        return arrayOf(getCustomStr(hours), getCustomStr(minutes), getCustomStr(seconds))
    }

    fun getDHMS(millis: Long): Array<Long> {
        val tmp = millis / 1000
        val day = tmp / (86_400)
        val hours = (tmp - day * 86_400) / 3_600
        val minutes = (tmp - day * 86_400 - hours * 3_600) / 60
        val seconds = (tmp - day * 86_400 - hours * 3_600 - minutes * 60)
        return arrayOf(day, hours, minutes, seconds)
    }

    fun getDHMSFormat(millis: Long): String {
        val sb = StringBuilder()
        val arr = getDHMS(millis)
        if (arr[0] > 0) {
            sb.append("${arr[0]}天")
        }
        if (arr[1] > 0) {
            sb.append("${arr[1]}小时")
        }
        if (arr[2] > 0) {
            sb.append("${arr[2]}分钟")
        }
        if (arr[3] > 0) {
            sb.append("${arr[3]}秒")
        }
        if (arr[0] == 1L && arr[1] <= 0 && arr[2] <= 0 && arr[3] <= 0) {
            return "24小时"
        }
        return sb.toString()
    }

    fun getCustomStr(int: Long): String {
        return if (int < 10) {
            "0$int"
        } else {
            "$int"
        }
    }
}