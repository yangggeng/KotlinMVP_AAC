package com.aiden.app.common.view.banner

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class IndicatorView : View {

    private var selectedColor = Color.BLACK
    private var unselectedColor = Color.WHITE
    private var dotRadius = 0f
    private var dotCount = 0
    private var margin = 10
    private var selectedPosition = 0
    private var marginBottom = 10
    private var dotWidth = 0f
    private var dotHeight = 0f
    private var paint: Paint = Paint()

    constructor(context: Context, attrs: AttributeSet? = null) : super(context, attrs) {
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        paint.color = unselectedColor
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (dotRadius > 0) {
            val width = (margin + dotRadius * 2) * dotCount - margin
            val height = marginBottom + (dotRadius * 2).toInt()
            setMeasuredDimension(width.toInt(), height)
        } else if (dotWidth > 0 && dotHeight > 0) {
            val width = (margin + dotWidth) * dotCount - margin
            val height = marginBottom + dotWidth
            setMeasuredDimension(width.toInt(), height.toInt())
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        var position = 0
        while (position < dotCount) {
            if (position == selectedPosition) {
                paint.color = selectedColor
            } else {
                paint.color = unselectedColor
            }
            if (dotRadius > 0) {
                canvas?.drawCircle((margin + dotRadius * 2) * position + dotRadius, dotRadius, dotRadius, paint)
            } else if (dotWidth > 0 && dotHeight > 0) {
                val rectF = RectF((margin + dotWidth) * position, 0f, (margin + dotWidth) * position + dotWidth, dotHeight)
                canvas?.drawRoundRect(rectF, dotHeight / 2, dotHeight / 2, paint)
            }
            position++
        }
    }

    fun setDotRadius(radius: Float): IndicatorView {
        this.dotRadius = radius
        return this
    }

    fun setSelectedColor(selectedColor: Int): IndicatorView {
        this.selectedColor = selectedColor
        return this
    }

    fun setUnselectedColor(unselectedColor: Int): IndicatorView {
        this.unselectedColor = unselectedColor
        return this
    }

    fun setDotCount(count: Int): IndicatorView {
        this.dotCount = count
        requestLayout()
        return this
    }

    fun setDotSize(width: Float, height: Float): IndicatorView {
        dotWidth = width
        dotHeight = height
        return this
    }

    fun setDotMargin(margin: Int): IndicatorView {
        this.margin = margin
        return this
    }

    fun setMarginBottom(marginBottom: Int): IndicatorView {
        this.marginBottom = marginBottom
        return this
    }

    fun changePosition(selectedPosition: Int): IndicatorView {
        this.selectedPosition = selectedPosition
        postInvalidate()
        return this
    }
}