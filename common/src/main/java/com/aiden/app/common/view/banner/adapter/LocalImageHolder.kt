package com.aiden.app.common.view.banner.adapter

import android.view.View
import android.widget.ImageView
import com.aiden.app.common.R

class LocalImageHolder(itemView: View, var scaleType: ImageView.ScaleType = ImageView.ScaleType.FIT_XY) : Holder<Int>(itemView) {

    private lateinit var imageView: ImageView

    override fun initView(itemView: View) {
        imageView = itemView.findViewById(R.id.iv_image)
    }

    override fun updateUI(data: Int) {
        imageView.scaleType = scaleType
        imageView.setImageResource(data)
    }
}