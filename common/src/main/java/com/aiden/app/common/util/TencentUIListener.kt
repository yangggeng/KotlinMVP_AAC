package com.aiden.app.common.util

import android.app.Activity
import com.aiden.app.common.event.EventMessage
import com.aiden.app.common.event.EventTag
import com.tencent.tauth.IUiListener
import com.tencent.tauth.Tencent
import com.tencent.tauth.UiError
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

/**
 * QQ登录回调
 *
 * @author yanggeng
 * @date 2019/2/14 下午1:57
 */
class TencentUIListener(var tencent: Tencent, var activity: Activity) : IUiListener {

    private val LOG_TAG = TencentUIListener::class.java.simpleName

    override fun onComplete(response: Any?) {
        val obj = response as JSONObject
        val openId = obj.getString("openid")
        val accessToken = obj.getString("access_token")
        LogUtils.d(LOG_TAG, "qq openId = ${openId}")
        val map = mutableMapOf<String, String>()
        map.put("openId", openId)
        map.put("accessToken", accessToken)
        EventBus.getDefault().post(EventMessage(EventTag.QQ_LOGIN_SUCCESS, map))
//        tencent.openId = openId
//        tencent.setAccessToken(accessToken, expires)
//
//        val qqToken = tencent.qqToken
//
//        val userInfo = UserInfo(activity, qqToken)
//        userInfo.getUserInfo(object : IUiListener {
//            override fun onComplete(response: Any?) {
//                LogUtils.d(LOG_TAG, "QQ登录成功！！！")
//                val resultMap = Gson().fromJson<HashMap<String, String>>(response?.toString(), HashMap::class.java)
//                resultMap.put("openid", openId)
//            }
//
//            override fun onCancel() {
//                EventBus.getDefault().post(EventMessage(EventTag.QQ_LOGIN_CANCEL))
//            }
//
//            override fun onError(p0: UiError?) {
//                EventBus.getDefault().post(EventMessage(EventTag.QQ_LOGIN_ERROR, p0?.toString()))
//            }
//        })
    }

    override fun onCancel() {
        EventBus.getDefault().post(EventMessage(EventTag.QQ_LOGIN_CANCEL))
    }

    override fun onError(p0: UiError?) {
        EventBus.getDefault().post(EventMessage(EventTag.QQ_LOGIN_ERROR, p0?.toString()))
    }
}