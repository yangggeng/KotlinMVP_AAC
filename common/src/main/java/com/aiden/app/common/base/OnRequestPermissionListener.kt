package com.aiden.app.common.base

interface OnRequestPermissionListener {

    fun onGranted()

    fun onDenied()
}