package com.aiden.app.common.constant

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/3/12
 * Email：ibelieve1210@163.com
 */
enum class DateFormatType(val fromContent: String) {
    Simply_DateOnly("yy/M/d"),
    Simply("yy/M/d hh:mm"),
    Detail_DateOnly("yyyy/MM/dd"),
    Detail("yyyy/MM/dd hh:mm"),
    Detail_WithSec("yyyy/MM/dd hh:mm:ss"),
    Detail_TimeOnly("hh:mm:ss"),
}