package com.aiden.app.common.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.util.List;

public class PackageUtil {

    public static boolean isWeixinAvailable(Context context) {
        return isAppAvailable(context, "com.tencent.mm");
    }

    public static boolean isQQClientAvailable(Context context) {
        return isAppAvailable(context, "com.tencent.mobileqq");
    }

    public static boolean isAliPayAvailable(Context context) {
        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }

    public static boolean isAMapAvailable(Context context) {
        return isAppAvailable(context, "com.autonavi.minimap");
    }

    public static boolean isBMapAvailable(Context context) {
        return isAppAvailable(context, "com.baidu.BaiduMap");
    }

    public static boolean isTencentMapAvailable(Context context) {
        return isAppAvailable(context, "com.tencent.map");
    }

    public static boolean isAppAvailable(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> installedPackages = manager.getInstalledPackages(0);
        if (installedPackages != null) {
            for (PackageInfo info : installedPackages) {
                if (info.packageName.equals(packageName))
                    return true;
            }
        }
        return false;
    }
}
