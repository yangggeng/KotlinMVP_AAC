package com.aiden.app.common.view.banner

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet

/**
 * 基于RecyclerView的ViewPager
 *
 * @author yanggeng
 * @date 2019/1/8 上午9:43
 */
class CTViewPager : RecyclerView {

    private val FLING_MAX_VELOCITY = 3000 // 最大顺时滑动速度
    private val mEnableLimitVelocity = true // 限制最大顺时滑动速度

    constructor(context: Context): super(context)

    constructor(context: Context, attrs: AttributeSet): super(context, attrs)

    override fun fling(velocityX: Int, velocityY: Int): Boolean {
        var x = velocityX
        var y = velocityY
        if (mEnableLimitVelocity) {
            x = solveVelocity(velocityX);
            y = solveVelocity(velocityY);
        }
        return super.fling(x, y)
    }

    private fun solveVelocity(velocity: Int): Int {
        if (velocity > 0) {
            return Math.max(velocity, FLING_MAX_VELOCITY)
        } else {
            return Math.max(velocity, -FLING_MAX_VELOCITY)
        }
    }
}