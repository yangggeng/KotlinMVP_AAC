package com.aiden.app.common.view.banner.listener

import android.support.v7.widget.RecyclerView

interface OnPageChangeListener {

    fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int)

    fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)

    fun onPageSelected(position: Int)
}