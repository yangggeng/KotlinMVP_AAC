package com.aiden.app.common.extension

import com.aiden.app.common.entity.TimeDateOnly
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

val YMDHMS = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA)

val YMD = SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)

val HMS = SimpleDateFormat("HH:mm:ss", Locale.CHINA)
val YMDHM = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA)

val YMD_Dot = SimpleDateFormat("yyyy.MM.dd", Locale.CHINA)

val YMD_China = SimpleDateFormat("yyyy年MM月dd日", Locale.CHINA)

val YMDHMS_China = SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss", Locale.CHINA)

fun Date.format(dateFormat: DateFormat): String = dateFormat.format(this)

fun Date.formatYMDHMS(): String = format(YMDHMS)

fun Date.formatYMD(): String = format(YMD)

fun Date.formatHMS(): String = format(HMS)

fun Date.formatYMDHM(): String = format(YMDHM)

fun Date.formatYMD_Dot(): String = format(YMD_Dot)

fun Date.getYMD(): List<Int> {
    val tmp = Calendar.getInstance()
    tmp.time = this
    return arrayListOf(tmp.get(Calendar.YEAR), tmp.get(Calendar.MONTH) + 1, tmp.get(Calendar.DATE))
}

fun Date.getTimeDateOnly(): TimeDateOnly {
    val tmp = Calendar.getInstance()
    tmp.time = this
    return TimeDateOnly(tmp.get(Calendar.YEAR).toString(), (tmp.get(Calendar.MONTH) + 1).toString(), tmp.get(Calendar.DATE).toString())
}

