package com.aiden.app.common.constant

/**
 * 埋点事件ID
 *
 * @author yanggeng
 * @date 2019/6/25 20:44
 */
object EventConstants {

    const val App_Start = "ClientStartSuccessful" // 客户端启动成功
    const val App_Home = "carOwnerPage" // App首页
    const val App_Community = "community" // 社区
    const val App_Shop = "store" // 进入商城页面
    const val App_Mine = "mine" // 进入我的页面
    const val App_Function = "userAggregationButton" // 进入聚合功能页
    const val App_SellerHome = "merchantsHomepage" // 进入商家接单首页

    const val Community_HotspotInformation = "hotspotInformation" // 热点资讯详情
    const val Community_CommonSenseOfCarUse = "commonSenseOfCarUse" // 用车常识

    const val Order_PaySuccess = "paySuccess" // 支付成功
    const val Order_ConfirmPayment = "confirmPayment" // 收银台确认支付
    const val Order_Submit = "submitOrders" // 跳转到收银台
    const val Order_ListToDetail = "myPageClickOrderdetail" // 订单列表进入订单详情
    const val Order_DetailToPay = "orderdetailClickGoToPay" // 订单详情页去付款
    const val Order_DetailUnPayToCancel = "orderdetailClickGoToCancel" // 待付款订单详情取消订单
    const val Order_CancelOrderConfirm = "cancelOrderClickSure" // 取消订单选择确定

    const val Shop_SearchResult = "mallSearchResult" // 跳转到搜索结果页
    const val Shop_Search = "mallSearch" // 跳转商城搜索页
    const val Shop_SingleToConfirm = "toSettleAccounts" // 商品去结算跳转到提交订单
    const val Shop_CartToConfirm = "settlement" // 购物车结算跳转到提交订单
    const val Shop_AddToCart = "addToCart" // 加入购物车
    const val Shop_Cart = "shoppingCart" // 进入购物车
    const val Shop_AddToCartSuccess = "addToCartSuccess" // 加入购物车成功
    const val Shop_RecommendProduct = "recommendCommodities" // 推荐商品进入详情页
    const val Shop_RecommendStore = "boutiqueShops" // 精品店铺进入详情页
    const val Shop_Banner = "homeRotationPicture" // 商城首页轮播图进入详情页
    const val Shop_ProductDetail = "CommodityDetailPage_Exhibition" // 商品详情页

    // 商品分类
    const val Shop_CategoryCarUse = "mallcar_accessories" // 汽车用品
    const val Shop_CategoryCarParts = "mallauto_spare_parts" // 汽车配件
    const val Shop_CategoryCarTools = "mallAutomotive_tools" // 汽保工具
    const val Shop_CategoryOil = "mallengine_oil" // 机油
    const val Shop_CategoryTire = "malltires_and_wheels" // 轮胎轮毂
    const val Shop_CategoryMaintain = "mallroutine_maintenance" // 常规保养
    const val Shop_CategoryBattery = "mall_accumulator" // 蓄电池
    const val Shop_CategoryBeauty = "mallAuto_Beatuty" // 美容装饰
    const val Shop_CategoryGlass = "mallautomotive_glass" // 汽车玻璃
    const val Shop_CategoryAll = "mallclassification" // 分类
    const val Shop_CategoryPaint = "mall_Insurance_and_Painting" // 钣金喷漆
    const val Shop_CategoryRepack = "mallautomobile_repacking" // 改装
    const val Shop_CategoryLocationRent = "mallLocation_for_rent" // 工位出租
    const val Shop_CategoryTechConsult = "malltechnology_consulting" // 技术咨询
    const val Shop_CategoryRepairShop = "mallrepair_shop" // 维修店
    const val Shop_CategoryRepairTech = "mallmaintenance_technician" // 维修技师
    const val Shop_CategorySecondaryMarket = "mallsecondary_market" // 二手市场
    const val Shop_CategoryInsurance = "mallinsurance_services" // 保险业务
    const val Shop_CategoryOther = "mallrests" // 其他

    const val MineStore_PersonalApply = "personalMerchantRegistrationDetermine" // 个人入驻提交申请
    const val MineStore_CompanyApply = "enterpriseMerchantRegistrationDetermine" // 企业入驻提交申请
    const val MineStore_EnterPersonalApply = "myPageClickPersonalMerchantRegistration" // 进入个人入驻申请
    const val MineStore_EnterCompanyApply = "myPageClickEnterpriseMerchantRegistration" // 进入企业入驻申请


    const val User_PersonalInfo = "myPageClickPersonalInformation" // 进入个人信息
    const val User_Setting = "myPageClickSetUp" // 设置
    const val User_MineStore = "myPageClickMyStore" // 我的店铺
    const val User_Feedback = "myPageClickFeedback" // 进入意见反馈
    const val User_Coupon = "myPageClickCoupon" // 进入用户优惠券列表
    const val User_CustomerService = "myPageClickCustomerService" // 进入客服中心
    const val User_Requirement = "myPageClickRequirements" // 我的需求
    const val User_Cart = "myPageClickShoppingCar" // 我的页面进入购物车
    const val User_Collection = " myPageClickCollection" // 我的页面进入收藏
    const val User_CollectionProduct = "collectionClickcommodity" // 收藏页点击商品
    const val User_CollectionArticle = "collectionClickArticle" // 收藏页点击文章
    const val User_Focus = "myPageClickAttention" // 我的页面进入关注
    const val User_FocusTechnician = "attentionClickTechnician" // 关注页点击技师
    const val User_FocusStore = "attentionClickStore" // 关注页点击店铺
    const val User_Order = "myPageClickOrder" // 我的页面进入订单列表
    const val User_Car = "myPageClickMyCar" // 我的页面进入爱车
    const val User_Plan = "myPageClickProgramme" // 我的页面进入方案
    const val User_Comment = "myPageClickEvaluation" // 我的页面进入评价
    const val User_AfterSale = "myPageClickAfterSale" // 我的页面进入售后

    const val Login_Register = "Register_Account" // 登录/注册页面
    const val Login_SendMessage = "Short_Message_Mobile_Originate" // 发送短信
    const val Login_SendMessageSuccess = "SMS_successfully_sent" // 短信发送成功
    const val Login_RegisterSuccess = "login_successful" // 登录成功

    const val Plan_Create = "programmeClickCreateNew" // 方案列表创建方案
    const val Plan_EditSave = "editProgrammeClickSave" // 编辑方案点击保存
    const val Plan_EditRecommend = "editProgrammeClickRecommend" // 编辑方案点击推荐
    const val Plan_Edit = "programmeClickEdit" // 历史方案点击编辑
    const val Plan_Use = "ProgrammeClickUse" // 历史方案点击使用
    const val Plan_Detail = "programmeClickDetail" // 历史方案点击详情

    const val Requirement_RespondedHistory = "merchantHomePageClickResponseRequirements" // 接单页点击已响应需求
    const val Requirement_SellerHomeRequirementDetail = "merchantHomePageClickRequirementsDetial" // 接单页点击跳转到需求详情
    const val Requirement_SelectType = "publishingRequirements" // 选择需求类型页
    const val Requirement_MaintainCreate = "maintenanceRequirements" // 维保需求页
    const val Requirement_MaintainPreview = "maintenanceRequirementsPreview" // 维保需求需求预览页
    const val Requirement_ProductCreate = "commodityRequirements" // 商品需求页
    const val Requirement_ProductPreview = "commodityRequirementsPreview" // 商品需求需求预览页
    const val Requirement_Save = "saveRequirements" // 保存需求
    const val Requirement_Publish = "releaseRequirements" // 发布需求

    const val Home_MineCar = "carOwnerPageClickMyGarage" // 首页点击我的车型库
    const val Home_NewStoreRecommend = "carOwnerPageClickNewStore" // 首页点击新店推荐
    const val Home_LocalStore = "carOwnerPageClickLocalStore" // 首页点击本地店铺
    const val Home_Article = "carOwnerPageClickArticle" // 首页点击精选好文
    const val Home_Position = "carOwnerPageClickPositioning" // 首页定位
    const val Home_Banner = "carOwnerPageClickslideshow" // 首页轮播图进入详情
    const val Home_RepairStore = "searchStore" // 找店铺
    const val Home_Technician = "searchTechnician" // 找技师

    const val IM_Chat = "imChat" // 进入im聊天页
    const val IM_EnterPlan = "imChatClickProgramme" // IM聊天页 点击进入方案页

    const val SellerHome_Position = "merchantHomePageClickPositioning" // 商户首页定位
    const val SellerHome_MineStore = "merchantHomePageClickMyStore" // 商家首页点击进入我的店铺\
    const val SellerHome_Banner = "merchantHomePageslideshow" // 商家首页轮播图进入详情页

}