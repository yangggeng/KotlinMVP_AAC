package com.aiden.app.common.http.config

import com.aiden.app.common.constant.PrefConstants
import com.aiden.app.common.util.SPUtils
import okhttp3.ConnectionPool
import java.io.File
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSocketFactory

object HttpGlobalConfig{

    @Deprecated("接口Host地址使用app的build.gradle进行配置")
    val API_HOST_LIST = mutableListOf("http://alpha.api.d-hu.cn:8100") // http://39.106.198.77:8100/
    val baseUrl = API_HOST_LIST[SPUtils.getInt(PrefConstants.PREF_HOST_INDEX)] // 默认api主机地址

    const val DEFAULT_TIMEOUT = 10000L // 默认超时时间（毫秒）
    const val CACHE_SIZE = 1024 * 1024 * 10L // 默认缓存存储控件大小（字节）
    const val HTTP_LOG_TAG = "----HTTP----"
    const val CACHE_MAX_AGE = 30 * 60 // 默认缓存最大生效时长
    const val PREFS_COOKIE = "Cookie_Prefs" // 默认cookie目录
    const val DEFAULT_MAX_IDLE_CONNECTIONS = 5 // 默认空闲连接数
    const val DEFAULT_KEEP_ALIVE_DURATION = 8L // 默认心跳间隔时长（秒）
    const val SUCCESS_CODE: Int = 0 // 默认请求成功code
    const val FAILURE_CODE: Int = 1 // 默认请求失败code
    const val SERVICE_EXCEPTION = 2 // 服务器异常
    const val NETWORK_EXCEPTION = 3 // 网络异常

    const val ENCRYPT_KEY = "202006250b4c43127ec02edce69f6a5d" // 网络请求签名的key

    fun params(): Map<String, String> {
        val params = HashMap<String, String>()
        // TODO 2018/12/13 添加公共参数

        return params
    }

    lateinit var sslSocketFactory: SSLSocketFactory
    lateinit var hostnameVerifier: HostnameVerifier
    lateinit var connectionPool: ConnectionPool
    var globalHeaders: Map<String, String> = LinkedHashMap()
    var isHttpCache: Boolean = true
    lateinit var httpCacheDirectory: File
    var isCookie: Boolean = true
    var retryDelayMills: Int = 5000
    var retryCount = 3

}