package com.aiden.app.common.http

class BaseResponse<T> {

    var errorCode: String = ""
    lateinit var errorMsg: String
    var data: T? = null
    var isSuccessful = false
}