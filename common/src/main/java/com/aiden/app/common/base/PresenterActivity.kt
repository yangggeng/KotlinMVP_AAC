package com.aiden.app.common.base

import android.content.Intent
import android.os.Bundle

abstract class PresenterActivity<P : BasePresenter<*>> : BaseActivity() {


    protected val mPresenter: P by lazy { presenter() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        view()
        addLifecycleObserver(mPresenter)
        onCreate()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mPresenter.onActivityResult(requestCode, resultCode, data)
    }

    protected abstract fun onCreate()

    protected abstract fun presenter(): P

    protected abstract fun view()


}