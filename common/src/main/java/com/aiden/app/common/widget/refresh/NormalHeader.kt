package com.aiden.app.common.widget.refresh

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import com.aiden.app.common.R
import com.scwang.smartrefresh.layout.api.RefreshHeader
import com.scwang.smartrefresh.layout.api.RefreshKernel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.constant.RefreshState
import com.scwang.smartrefresh.layout.constant.SpinnerStyle

/**
 * 公共下拉刷新Header
 *
 * @author yanggeng
 * @date 2019/6/25 15:26
 */
class NormalHeader @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttrs: Int = 0) : FrameLayout(context, attrs, defStyleAttrs), RefreshHeader {

    private var ivRefresh: ImageView
    private var animationDrawable: AnimationDrawable
    private var selfView: View

    init {
        View.inflate(context, R.layout.refresh_view, this)
        selfView = this
        ivRefresh = selfView.findViewById(R.id.iv_refresh)
        ivRefresh.setImageResource(R.drawable.comm_refresh)
        animationDrawable = ivRefresh.drawable as AnimationDrawable
    }

    override fun getView(): View {
        return this
    }

    override fun getSpinnerStyle(): SpinnerStyle {
        return SpinnerStyle.Translate
    }

    override fun onStartAnimator(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {
        animationDrawable.start()
    }

    override fun onFinish(refreshLayout: RefreshLayout, success: Boolean): Int {
        if (animationDrawable.isRunning) {
            animationDrawable.stop()
        }
        return 200
    }

    override fun onStateChanged(refreshLayout: RefreshLayout, oldState: RefreshState, newState: RefreshState) {
        when (newState) {
            RefreshState.None, RefreshState.PullDownToRefresh -> {
                // 下拉过程
                animationDrawable.stop()
            }
            RefreshState.ReleaseToRefresh -> {
                // 释放立即刷新
            }

            RefreshState.Refreshing -> {
                // 刷新中
                animationDrawable.start()
            }
        }
    }

    override fun onInitialized(kernel: RefreshKernel, height: Int, maxDragHeight: Int) {

    }

    override fun onHorizontalDrag(percentX: Float, offsetX: Int, offsetMax: Int) {

    }

    override fun onReleased(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {

    }


    override fun setPrimaryColors(vararg colors: Int) {

    }


    override fun onMoving(isDragging: Boolean, percent: Float, offset: Int, height: Int, maxDragHeight: Int) {
    }

    override fun isSupportHorizontalDrag(): Boolean {
        return false
    }
}