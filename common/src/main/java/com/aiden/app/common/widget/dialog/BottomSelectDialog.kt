package com.aiden.app.common.widget.dialog

import android.app.Activity
import android.widget.TextView
import com.aiden.app.common.R
import com.aiden.app.common.util.LogUtils
import com.lxj.xpopup.core.BottomPopupView

/**
 * 底部弹出的两项选择对话框，比如：相机相册，男女 等
 *
 * @author yanggeng
 * @date 2019/2/27 下午4:13
 */
class BottomSelectDialog (var activity: Activity) : BottomPopupView(activity) {

    private val TAG = BottomSelectDialog::class.java.simpleName
    private lateinit var tvText1: TextView
    private lateinit var tvText2: TextView
    private lateinit var tvCancel: TextView
    private var text1: String = ""
    private var text2: String = ""
    private var cancel: String = "取消"
    private var onItemClickListener: OnItemClickListener?= null

    override fun getImplLayoutId(): Int {
        return R.layout.layout_bottom_dialog
    }

    override fun onCreate() {
        super.onCreate()
        tvText1 = findViewById(R.id.tv_text1)
        tvText2 = findViewById(R.id.tv_text2)
        if (!text1.isEmpty()) {
            tvText1.text = text1
        }
        tvText1.setOnClickListener {
            if (onItemClickListener != null) {
                onItemClickListener?.onItemClick(0)
                dismiss()
            } else {
                LogUtils.e(TAG, "Please set onItemClickListener!!!")
            }
        }
        if (!text2.isEmpty()) {
            tvText2.text = text2
        }
        tvText2.setOnClickListener {
            if (onItemClickListener != null) {
                onItemClickListener?.onItemClick(1)
                dismiss()
            } else {
                LogUtils.e(TAG, "Please set onItemClickListener!!!")
            }
        }
        tvCancel = findViewById(R.id.tv_cancel)
        tvCancel.text = cancel
        tvCancel.setOnClickListener {
            if (onItemClickListener != null) {
                onItemClickListener?.onItemClick(2)
                dismiss()
            } else {
                LogUtils.e(TAG, "Please set onItemClickListener!!!")
            }
        }
    }

    fun setText(text1: String, text2: String, cancel: String): BottomSelectDialog {
        this.text1 = text1
        this.text2 = text2
        this.cancel = cancel
        return this
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener): BottomSelectDialog {
        this.onItemClickListener = onItemClickListener
        return this
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}