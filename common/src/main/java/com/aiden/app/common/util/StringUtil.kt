package com.aiden.app.common.util

import java.util.regex.Pattern

fun double2Long(source: Double): Long {
    val sourceStr = source.toString()
    val floatCount = sourceStr.length - 1 - sourceStr.indexOf(".")
    if (floatCount < sourceStr.length) {
        var result = sourceStr.toDouble()
        for (i in 0 until floatCount) {
            result *= 10
        }
        return result.toLong()
    }
    return sourceStr.toLong()
}

fun isIdCard(source: String): Boolean {
    val regIdCard = "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}\$|^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)\$"
    return Pattern.compile(regIdCard).matcher(source).matches()
}