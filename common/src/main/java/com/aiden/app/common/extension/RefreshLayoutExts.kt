package com.aiden.app.common.extension

import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.constant.RefreshState

fun RefreshLayout.isRefreshing(): Boolean {
    return this.state == RefreshState.Refreshing
}

fun RefreshLayout.isLoading(): Boolean {
    return this.state == RefreshState.Loading
}