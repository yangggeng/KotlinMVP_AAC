package com.aiden.app.common.base

import com.aiden.app.common.util.LogUtils

/**
 * fragment的presenter
 *
 * @author yanggeng
 * @date 2019/4/15 14:28
 */
open class BaseFragmentPresenter<V : IBaseView> : BaseGenericLifecycleObserver() {

    private val LOG_TAG = BaseFragmentPresenter::class.java.simpleName

    protected lateinit var view: V

    fun loadView(view: V) {
        LogUtils.d(LOG_TAG, "loadView")
        this.view = view
    }

    override fun onCreate() {
        LogUtils.d(LOG_TAG, "onCreate")
    }

    /**
     * 继承该方法为viewmodel添加监听
     */
    override fun onStart() {
        LogUtils.d(LOG_TAG, "onStart")
    }

    override fun onResume() {
        LogUtils.d(LOG_TAG, "onResume")
    }

    override fun onPause() {
        LogUtils.d(LOG_TAG, "onPause")
    }

    override fun onStop() {
        LogUtils.d(LOG_TAG, "onStop")
    }

    override fun onDestroy() {
        LogUtils.d(LOG_TAG, "onDestroy")
    }

    override fun onAny() {
        LogUtils.d(LOG_TAG, "onAny")
    }
}