package com.aiden.app.common.util

import android.app.Activity
import android.os.Bundle
import com.aiden.app.common.base.BaseApplication
import com.tencent.connect.share.QQShare
import com.tencent.tauth.IUiListener

/**
 * QQ登录、分享工具类
 *
 * @author yanggeng
 * @date 2019/2/14 下午1:54
 */
class QQUtils {

    companion object {
        val INSTANCE = QQUtils()
    }

    private var tencent = BaseApplication.INSTANCE.getTencent()
    private lateinit var uiListener: TencentUIListener

    fun login(activity: Activity) {
        uiListener = TencentUIListener(tencent, activity)
        tencent.login(activity, "all", uiListener)
    }

    fun shareFriends(activity: Activity, title: String, summary: String = "", targetUrl: String, imageUrl: String = "", appName: String = "", shareListener: IUiListener) {
        val params = Bundle()
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_DEFAULT)
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title)
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, summary)
        params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, targetUrl)
        params.putString(QQShare.SHARE_TO_QQ_APP_NAME, appName)
        params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl)
        tencent.shareToQQ(activity, params, shareListener)
    }

    fun shareQQZone(activity: Activity, title: String, summary: String = "", targetUrl: String, imageUrls: ArrayList<String> = ArrayList(), appName: String = "", shareListener: IUiListener) {
        val params = Bundle()
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_FLAG_QZONE_AUTO_OPEN)
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title)
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, summary)
        params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, targetUrl)
        params.putStringArrayList(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrls)
        params.putString(QQShare.SHARE_TO_QQ_APP_NAME, appName)
        tencent.shareToQzone(activity, params, shareListener)
    }
}