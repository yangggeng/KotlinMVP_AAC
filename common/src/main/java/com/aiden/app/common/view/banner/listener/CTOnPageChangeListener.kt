package com.aiden.app.common.view.banner.listener

import android.support.v7.widget.RecyclerView
import com.aiden.app.common.view.banner.IndicatorView

/**
 * 更新轮播图指示器位置
 *
 * @author yanggeng
 * @date 2019/1/8 下午2:55
 */
class CTOnPageChangeListener(var indicatorView: IndicatorView): OnPageChangeListener {

    private var onPageChangeListener: OnPageChangeListener ?= null


    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        if (onPageChangeListener != null) onPageChangeListener?.onScrollStateChanged(recyclerView, newState)
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (onPageChangeListener != null) onPageChangeListener?.onScrolled(recyclerView, dx, dy)
    }

    override fun onPageSelected(position: Int) {
        indicatorView.changePosition(position)
        if (onPageChangeListener != null) onPageChangeListener?.onPageSelected(position)
    }

    fun setOnPageChangeListener(onPageChangeListener: OnPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener
    }
}