package com.aiden.app.common.widget.address

import com.contrarywind.interfaces.IPickerViewData
import java.io.Serializable

class AddressJsonBean(var name: String, var city: List<CityBean>) : IPickerViewData, Serializable {

    override fun getPickerViewText(): String {
        return name
    }


}