package com.aiden.app.common.view;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * 自动上下轮播文本框
 * Created by yanggeng on 2017/8/2.
 */

public class AutoScrollTextView extends TextSwitcher implements ViewSwitcher.ViewFactory {

    private static final int FLAG_START_SCROLL = 0x101;

    private static final int FLAG_STOP_SCROLL = 0x102;

    private float textSize = 16f;
    private int padding = 5;
    private int textColor = Color.BLACK;

    private OnItemClickListener onItemClickListener;
    private OnScrolledOnceListener onScrolledOnceListener;
    private Context context;
    private int currentItem = -1;
    private ArrayList<String> textList;
    private ScrollHandler handler = new ScrollHandler(this);
    private boolean isScrolling = false;
    private long duration = 2000L;

    public AutoScrollTextView(Context context) {
        super(context);
        this.context = context;
    }

    public AutoScrollTextView(Context context, AttributeSet attr) {
        super(context, attr);
        this.context = context;
        textList = new ArrayList<>();
    }

    @Override
    public View makeView() {
        TextView tv = new TextView(context);
        tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);
        tv.setMaxLines(1);
        tv.setEllipsize(TextUtils.TruncateAt.END);
        tv.setTextSize(textSize);
        tv.setTextColor(textColor);
        tv.setPadding(padding, padding, padding, padding);
        tv.setClickable(true);
        tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null && textList.size() > 0 && currentItem != -1) {
                    onItemClickListener.onItemClick(currentItem % textList.size());
                }
            }
        });
        return tv;
    }

    public AutoScrollTextView setTextStyle(float textSize, int padding, int textColor) {
        this.textSize = textSize;
        this.padding = padding;
        this.textColor = textColor;
        return this;
    }

    /**
     * 设置动画时间
     *
     * @param animDuration
     */
    public AutoScrollTextView setAnimDuration(long animDuration) {
        setFactory(this);
        Animation inAnim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0);
        inAnim.setDuration(animDuration);
        inAnim.setInterpolator(new AccelerateInterpolator());
        Animation outAnim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -1);
        outAnim.setDuration(animDuration);
        outAnim.setInterpolator(new AccelerateInterpolator());
        setInAnimation(inAnim);
        setOutAnimation(outAnim);
        return this;
    }

    public AutoScrollTextView setAnimation(Animation inAnim, Animation outAnim) {
        setInAnimation(inAnim);
        setOutAnimation(outAnim);
        return this;
    }

    private void refresh(int flag) {
        switch (flag) {
            case FLAG_START_SCROLL:
                if (textList.size() > 0) {
                    currentItem++;
                    setText(textList.get(currentItem % textList.size()));
                    if (currentItem == textList.size() - 1) {
                        if (onScrolledOnceListener != null) {
                            onScrolledOnceListener.onScrolledOnce();
                        }
                    }
                }
                if (!isScrolling) {
                    isScrolling = true;
                }
                handler.sendEmptyMessageDelayed(FLAG_START_SCROLL, duration);
                break;
            case FLAG_STOP_SCROLL:
                if (isScrolling) {
                    isScrolling = false;
                }
                handler.removeMessages(FLAG_START_SCROLL);
                break;
        }
    }

    /**
     * 设置滚动间隔时间
     *
     * @param mills
     */
    public AutoScrollTextView setScrollDuration(long mills) {
        this.duration = mills;
        return this;
    }

    /**
     * 设置数据源
     *
     * @param textList
     */
    public AutoScrollTextView setTextList(List<String> textList) {
        this.textList.clear();
        this.textList.addAll(textList);
        currentItem = -1;
        return this;
    }

    /**
     * 开始滚动
     */
    public void startScroll() {
        if (!textList.isEmpty()) {
            handler.sendEmptyMessage(FLAG_START_SCROLL);
        }
    }

    public boolean isScrolling() {
        return isScrolling;
    }

    /**
     * 停止滚动
     */
    public void stopScroll() {
        handler.sendEmptyMessage(FLAG_STOP_SCROLL);
    }

    public AutoScrollTextView setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        return this;
    }

    public AutoScrollTextView setOnScrolledOnceListener(OnScrolledOnceListener onScrolledOnceListener) {
        this.onScrolledOnceListener = onScrolledOnceListener;
        return this;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface OnScrolledOnceListener {
        void onScrolledOnce();
    }

    private static class ScrollHandler extends Handler {
        private WeakReference<AutoScrollTextView> weakReference;

        ScrollHandler(AutoScrollTextView textView) {
            weakReference = new WeakReference<>(textView);
        }

        public void setDuration() {

        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            AutoScrollTextView temp = weakReference.get();
            if (temp != null) {
                temp.refresh(msg.what);
            }
        }
    }
}
