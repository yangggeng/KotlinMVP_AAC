package com.aiden.app.common.util

import android.annotation.SuppressLint
import android.os.CountDownTimer
import android.widget.TextView

/**
 * 获取验证码倒计时
 *
 * @author yanggeng
 * @date 2019/1/3 下午4:16
 */
class CountDownTimer(var textView: TextView, millsInFuture: Long = 120000, countDownInterval: Long = 1000, var resendText: String = "重新获取验证码") : CountDownTimer(millsInFuture, countDownInterval) {

    override fun onFinish() {
        textView.text = resendText
        textView.isClickable = true //重新获得点击
    }

    @SuppressLint("SetTextI18n")
    override fun onTick(millisUntilFinished: Long) {
        textView.isClickable = false
        textView.text = "${millisUntilFinished / 1000}s"
    }
}