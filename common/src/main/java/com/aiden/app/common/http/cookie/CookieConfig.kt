package com.aiden.app.common.http.cookie

import android.content.Context
import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl

/**
 *
 * OkHttp Cookie配置
 * @author yangeng
 * @date 2018/12/14 下午2:58
 */
class CookieConfig(context: Context): CookieJar {

    private val cookieStore: CookieShop = CookieShop(context)

    override fun saveFromResponse(url: HttpUrl, cookies: MutableList<Cookie>) {
        if (cookies.size > 0) {
            for (item in cookies) {
                cookieStore.add(url, item)
            }
        }
    }

    override fun loadForRequest(url: HttpUrl): List<Cookie> {
        return cookieStore.get(url)
    }
}