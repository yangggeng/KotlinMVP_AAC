package com.aiden.app.common.util

import com.google.gson.Gson

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/3/19
 * Email：ibelieve1210@163.com
 */

object JsonUtils {
    /**
     * 将 JSON 字符串解析成 JavaBean
     */
    fun <T> parseJson2Bean(classOfT: Class<T>, jsonStr: String): T {
        val gsonParse = Gson()
        return gsonParse.fromJson<T>(jsonStr, classOfT)
    }

    /**
     * 将 Java 类封装成 JSON 字符串
     */
    fun createJsonString(value: Any): String {
        return Gson().toJson(value)
    }
}
