package com.aiden.app.common.base

import android.arch.lifecycle.LiveData

interface IBaseViewModel<T> {
    fun getData(): LiveData<T>
}