package com.aiden.app.common.view.banner

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView
import android.view.ViewTreeObserver
import com.aiden.app.common.util.LogUtils
import com.aiden.app.common.view.banner.adapter.CTAdapter
import com.aiden.app.common.view.banner.listener.OnPageChangeListener

class CTLoopScrollHelper {

    private val TAG = CTLoopScrollHelper::class.java.simpleName

    private lateinit var recyclerView: CTViewPager
    var pagePadding = 0
    var leftCardWidth = 0
    var firstItemPosition = 0
    private var pagerSnapHelper = PagerSnapHelper()
    private var onPageChangeListener: OnPageChangeListener ?= null

    fun attachToRecyclerView(recyclerView: CTViewPager) {
        this.recyclerView = recyclerView
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView1: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView1, newState)
                var position = getCurrentItem()
                val adapter = recyclerView.adapter as CTAdapter<*>
                val count = adapter.getRealItemCount()
                if (adapter.canLoop) {
                    if (position < count) {
                        position += count
                        setCurrentItem(position)
                    } else if (position >= count * 2) {
                        position -= count
                        setCurrentItem(position)
                    }
                }
                if (onPageChangeListener != null) {
                    onPageChangeListener?.onScrollStateChanged(recyclerView, newState)
                    if (count != 0) {
                        onPageChangeListener?.onPageSelected(position % count)
                    }
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (onPageChangeListener != null) {
                    onPageChangeListener?.onScrolled(recyclerView, dx, dy)
                }
            }
        })
        initWidth()
        pagerSnapHelper.attachToRecyclerView(recyclerView)
    }

    private fun initWidth() {
        recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                scrollToPosition(firstItemPosition)
            }
        })
    }

    fun setCurrentItem(position: Int, smoothScroll: Boolean = false) {
        if (smoothScroll) {
            recyclerView.smoothScrollToPosition(position)
        } else {
            scrollToPosition(position)
        }
    }

    fun scrollToPosition(position: Int) {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        linearLayoutManager.scrollToPositionWithOffset(position, pagePadding + leftCardWidth)
    }

    fun getCurrentItem(): Int {
        try {
            val layoutManager = recyclerView.layoutManager
            val view = pagerSnapHelper.findSnapView(layoutManager)
            if (view != null) {
                return layoutManager?.getPosition(view)!!
            }
        } catch (e: NullPointerException) {
            LogUtils.e(TAG, e.message!!)
        }
        return 0
    }

    fun getRealItemCount(): Int {
        val adapter = recyclerView.adapter as CTAdapter<*>
        return adapter.getRealItemCount()
    }

    fun setOnPageChangeListener(onPageChangeListener: OnPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener
    }
}