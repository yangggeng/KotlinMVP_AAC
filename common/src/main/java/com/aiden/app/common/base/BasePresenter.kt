package com.aiden.app.common.base

import android.content.Intent
import com.aiden.app.common.util.LogUtils

open class BasePresenter<V : IBaseView> : LifecycleCallback {

    private val LOG_TAG = BasePresenter::class.java.simpleName

    protected lateinit var view: V

    fun loadView(view: V) {
        this.view = view
    }

    override fun onCreate() {
        loadView(view)
        LogUtils.d(LOG_TAG, "onCreate")
    }

    override fun onStart() {
        LogUtils.d(LOG_TAG, "onStart")
    }

    override fun onResume() {
        LogUtils.d(LOG_TAG, "onResume")
    }

    override fun onPause() {
        LogUtils.d(LOG_TAG, "onPause")
    }

    override fun onStop() {
        LogUtils.d(LOG_TAG, "onStop")
    }

    override fun onDestroy() {
        LogUtils.d(LOG_TAG, "onDestroy")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    }
}