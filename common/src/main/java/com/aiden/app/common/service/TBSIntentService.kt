package com.aiden.app.common.service

import android.content.Context
import android.content.Intent
import android.support.v4.app.JobIntentService
import com.aiden.app.common.util.LogUtils
import com.tencent.smtt.sdk.QbSdk

/**
 *
 * 初始化X5内核service
 * @author yanggeng
 * @date 2019/1/2 下午1:49
 */
class TBSIntentService : JobIntentService() {
    private val TAG = TBSIntentService::class.java.simpleName

    override fun onHandleWork(p0: Intent) {
        initX5Web()
    }

    companion object {
        private val SERVICE_ID = 0x999

        fun enqueueWork(context: Context, intent: Intent) {
            enqueueWork(context, TBSIntentService::class.java, SERVICE_ID, intent)
        }
    }


    internal var cb: QbSdk.PreInitCallback = object : QbSdk.PreInitCallback {
        override fun onCoreInitFinished() {
            LogUtils.e(TAG, "onCoreInitFinished")
        }

        override fun onViewInitFinished(b: Boolean) {
            LogUtils.e(TAG, "x5 webview init $b")
        }
    }

    private fun initX5Web() {
        if (!QbSdk.isTbsCoreInited()) {
            QbSdk.preInit(applicationContext, cb)
        }
        QbSdk.initX5Environment(applicationContext, cb)
    }


}
