package com.aiden.app.common.widget.address;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.FrameLayout;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.aiden.app.common.util.AssetUtil;
import com.aiden.app.common.util.SystemUtil;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class AddressDialog {

    private static final String TAG = AddressDialog.class.getSimpleName();
    private static final int MSG_LOAD_DATA = 0x0001;
    private static final int MSG_LOAD_SUCCESS = 0x0002;
    private static final int MSG_LOAD_FAILED = 0x0003;
    public static final int TYPE_3 = 0x0004;
    public static final int TYPE_2 = 0x0005;
    private ArrayList<AddressJsonBean> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private ArrayList<ArrayList<ArrayList<String>>> options3Items = new ArrayList<>();
    private static boolean isLoaded = false;
    private Thread thread;
    private Context context;
    private OnSelectedListener onSelectedListener;
    private OnDataLoadListener onDataLoadListener;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_LOAD_DATA:
                    thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            initJsonData();
                        }
                    });
                    thread.start();
                    break;
                case MSG_LOAD_SUCCESS:
                    isLoaded = true;
                    if (onDataLoadListener != null) {
                        onDataLoadListener.onDataLoad();
                    }
                    break;
                case MSG_LOAD_FAILED:
                    isLoaded = false;
                    break;
            }
        }
    };

    public AddressDialog(Context context) {
        this.context = context;
        mHandler.sendEmptyMessage(MSG_LOAD_DATA);
    }

    public void show(int type) {
        if (type == TYPE_3) {
            final OptionsPickerView pvOptions = new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
                @Override
                public void onOptionsSelect(int options1, int options2, int options3, View v) {
                    String province = options1Items.size() > 0 ? options1Items.get(options1).getPickerViewText() : "";
                    String city = options2Items.size() > 0 && options2Items.get(options1).size() > 0 ? options2Items.get(options1).get(options2) : "";
                    String area = options2Items.size() > 0 && options3Items.get(options1).size() > 0 && options3Items.get(options1).get(options2).size() > 0 ? options3Items.get(options1).get(options2).get(options3) : "";
                    if (onSelectedListener != null) {
                        onSelectedListener.onSelected(province, city, area);
                    }
                }
            })
                    .setTitleText("")
                    .build();
            pvOptions.setPicker(options1Items, options2Items, options3Items);
            if (SystemUtil.INSTANCE.isNavigationBarVisible(context)) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) pvOptions.getDialogContainerLayout().getLayoutParams();
                lp.bottomMargin = SystemUtil.INSTANCE.getNavigationHeight(context);
                pvOptions.getDialogContainerLayout().setLayoutParams(lp);
            }
            if (isLoaded) {
                pvOptions.show();
            } else {
                onDataLoadListener = new OnDataLoadListener() {
                    @Override
                    public void onDataLoad() {
                        pvOptions.show();
                    }
                };
            }
        } else if (type == TYPE_2) {
            final OptionsPickerView pvOptions = new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
                @Override
                public void onOptionsSelect(int options1, int options2, int options3, View v) {
                    String province = options1Items.size() > 0 ? options1Items.get(options1).getPickerViewText() : "";
                    String city = options2Items.size() > 0 && options2Items.get(options1).size() > 0 ? options2Items.get(options1).get(options2) : "";
                    if (onSelectedListener != null) {
                        onSelectedListener.onSelected(province, city, "");
                    }
                }
            })
                    .setTitleText("")
                    .build();
            pvOptions.setPicker(options1Items, options2Items);
            if (SystemUtil.INSTANCE.isNavigationBarVisible(context)) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) pvOptions.getDialogContainerLayout().getLayoutParams();
                lp.bottomMargin = SystemUtil.INSTANCE.getNavigationHeight(context);
                pvOptions.getDialogContainerLayout().setLayoutParams(lp);
            }
            if (isLoaded) {
                pvOptions.show();
            } else {
                onDataLoadListener = new OnDataLoadListener() {
                    @Override
                    public void onDataLoad() {
                        pvOptions.show();
                    }
                };
            }
        }

    }

    public void setOnSelectedListener(OnSelectedListener onSelectedListener) {
        this.onSelectedListener = onSelectedListener;
    }

    private void initJsonData() {

        String jsonData = AssetUtil.INSTANCE.getJson(context, "province.json");

        ArrayList<AddressJsonBean> jsonBeans = parseJson(jsonData);

        options1Items = jsonBeans;
        for (int i = 0; i < jsonBeans.size(); i++) {
            ArrayList<String> cityList = new ArrayList<>();
            ArrayList<ArrayList<String>> provinceAreaList = new ArrayList<>();

            for (int j = 0; j < jsonBeans.get(i).getCity().size(); j++) {
                cityList.add(jsonBeans.get(i).getCity().get(j).getName());
                ArrayList<String> cityAreaList = new ArrayList<>();

                cityAreaList.addAll(jsonBeans.get(i).getCity().get(j).getArea());
                provinceAreaList.add(cityAreaList);
            }

            options2Items.add(cityList);
            options3Items.add(provinceAreaList);
        }
        mHandler.sendEmptyMessage(MSG_LOAD_SUCCESS);
    }

    private ArrayList<AddressJsonBean> parseJson(String json) {
        ArrayList<AddressJsonBean> detail = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(json);
            Gson gson = new Gson();
            for (int i = 0; i < data.length(); i++) {
                AddressJsonBean entity = gson.fromJson(data.optJSONObject(i).toString(), AddressJsonBean.class);
                detail.add(entity);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            mHandler.sendEmptyMessage(MSG_LOAD_FAILED);
        }
        return detail;
    }

    public interface OnSelectedListener {
        void onSelected(String province, String city, String area);
    }

    private interface OnDataLoadListener {
        void onDataLoad();
    }
}
