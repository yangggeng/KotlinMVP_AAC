package com.aiden.app.common.util

import android.util.Log
import com.aiden.app.common.BuildConfig

object LogUtils {

    private val PREFIX = "----DAHU----";
    val debug = BuildConfig.DEBUG

    fun e(title: String, content: String) {
        if (debug) {
            Log.e(title, PREFIX + content)
        }
    }

    fun e(title: String, content: String, tr: Throwable) {
        if (debug) {
            Log.d(title, PREFIX + content, tr)
        }
    }

    fun d(title: String, content: String) {
        if (debug) {
            Log.d(title, PREFIX + content)
        }
    }

    fun d(title: String, content: String, tr: Throwable) {
        if (debug) {
            Log.d(title, PREFIX + content, tr)
        }
    }

    fun i(title: String, content: String) {
        if (debug) {
            Log.i(title, PREFIX + content)
        }
    }

    fun w(title: String, content: String) {
        if (debug) {
            Log.w(title, PREFIX + content)
        }
    }
}