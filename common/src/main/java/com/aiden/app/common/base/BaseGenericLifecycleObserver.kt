package com.aiden.app.common.base

import android.arch.lifecycle.GenericLifecycleObserver
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import com.aiden.app.common.util.LogUtils

/**
 * fragment生命周期监听
 *
 * @author yanggeng
 * @date 2019/4/15 14:28
 */
abstract class BaseGenericLifecycleObserver : GenericLifecycleObserver {

    private val TAG = BaseGenericLifecycleObserver::class.java.simpleName
    private var isOnStart = false

    override fun onStateChanged(source: LifecycleOwner?, event: Lifecycle.Event?) {
        when (event) {
            Lifecycle.Event.ON_CREATE -> onCreate()
            Lifecycle.Event.ON_START -> {
                if (!isOnStart) {
                    onStart()
                    isOnStart = true
                }
            }
            Lifecycle.Event.ON_RESUME -> onResume()
            Lifecycle.Event.ON_PAUSE -> onPause()
            Lifecycle.Event.ON_STOP -> onStop()
            Lifecycle.Event.ON_DESTROY -> onDestroy()
            Lifecycle.Event.ON_ANY -> onAny()
            else -> LogUtils.d(TAG, "unknown event")
        }
    }

    abstract fun onCreate()

    abstract fun onStart()

    abstract fun onResume()

    abstract fun onPause()

    abstract fun onStop()

    abstract fun onDestroy()

    abstract fun onAny()
}