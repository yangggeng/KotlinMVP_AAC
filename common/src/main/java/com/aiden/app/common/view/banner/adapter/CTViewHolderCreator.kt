package com.aiden.app.common.view.banner.adapter

import android.view.View

interface CTViewHolderCreator<T> {

    fun createHolder(itemView: View): Holder<T>

    fun getLayoutId(): Int
}