package com.aiden.app.common.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

open class BaseViewModel<T>: ViewModel(), IBaseViewModel<T> {

    var liveData: MutableLiveData<T> = MutableLiveData()

    override fun getData(): MutableLiveData<T> {
        return liveData
    }
}