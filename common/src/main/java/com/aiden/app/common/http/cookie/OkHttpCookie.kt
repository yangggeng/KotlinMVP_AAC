package com.aiden.app.common.http.cookie

import okhttp3.Cookie
import java.io.Serializable

/**
 *
 * Cookie序列化对象
 * @author yanggeng
 * @date 2018/12/13 上午9:57
 */
class OkHttpCookie(private val cookie: Cookie): Serializable {

    private var clientCookie: Cookie ?= null

    fun getCookies(): Cookie {
        var bestCookie = cookie
        if (clientCookie != null) {
            bestCookie = clientCookie!!
        }
        return bestCookie
    }


}