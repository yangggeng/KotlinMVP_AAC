package com.aiden.app.common.http.converter

import com.aiden.app.common.http.BaseResponse
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import okhttp3.ResponseBody
import retrofit2.Converter

/**
 * 对请求到的数据做异常处理，重写了 {@link GsonResponseBodyConverter}，并重写相关调用类
 * @author aiden@tronsis.com
 * @date 2018/8/9 17:13
 */
class GsonResponseBodyConverter<T>(var gson: Gson, var adapter: TypeAdapter<T>) : Converter<ResponseBody, T> {

    override fun convert(value: ResponseBody?): T? {

        try {
            val t = adapter.fromJson(value!!.charStream())
            if (t is BaseResponse<*>) {
                return t
            } else {
                return t
            }

        } finally {
            value!!.close()
        }
    }
}