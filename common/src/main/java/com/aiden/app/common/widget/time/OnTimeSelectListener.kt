package com.aiden.app.common.widget.time

import android.view.View
import java.util.*

interface OnTimeSelectListener {

    fun onTimeSelect(date: Date, selectedExtraIndex: Int, v: View?)
}