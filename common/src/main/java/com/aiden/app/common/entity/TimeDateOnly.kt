package com.aiden.app.common.entity

import com.aiden.app.common.extension.isNil
import com.aiden.app.common.extension.toIntNoException
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

/**
 * Descriptions：年月日
 * <p>
 * Author：ChenME
 * Date：2019/5/21
 * Email：ibelieve1210@163.com
 */
data class TimeDateOnly(
        var year: String = "",
        var month: String = "",
        var day: String = ""
) : Serializable {

    fun format2String(): String {
        if (year.isNil() || month.isNil() || day.isNil()) {
            return ""
        }
        return "$year-$month-$day"
    }

    fun format2TimeMillis(): TimeMillis {
        if (year.isNil() || month.isNil() || day.isNil()) {
            return TimeMillis()
        }
        return TimeMillis((SimpleDateFormat("yyyy-M-d").parse(format2String()).time / 1000L).toString(), "")
    }

    fun isBeforeToday(): Boolean {
        val cl = Calendar.getInstance()
        if (year.toIntNoException() == cl.get(Calendar.YEAR)) { // 年相等
            if ((month.toIntNoException() == cl.get(Calendar.MONTH) + 1)) { // 月相等
                if (day.toIntNoException() < cl.get(Calendar.DAY_OF_MONTH)) {
                    return true
                }
            } else if ((month.toIntNoException() < cl.get(Calendar.MONTH) + 1)) {
                return true
            }
        } else if (year.toIntNoException() < cl.get(Calendar.YEAR)) {
            return true
        }
        return false
    }

    companion object {
        fun fromLong(mills: Long): TimeDateOnly {
            val cal = Calendar.getInstance()
            cal.timeInMillis = mills
            return TimeDateOnly(cal.get(Calendar.YEAR).toString(), (cal.get(Calendar.MONTH) + 1).toString(), cal.get(Calendar.DAY_OF_MONTH).toString())
        }
    }

}