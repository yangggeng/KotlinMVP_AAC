package com.aiden.app.common.util

import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorListener
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.View

/**
 * Created by yanggeng on 2017/8/30.
 */

object AnimatorUtil {

    val FAST_OUT_SLOW_IN_INTERPOLATOR = LinearOutSlowInInterpolator()

    // 显示view
    fun scaleShow(view: View, viewPropertyAnimatorListener: ViewPropertyAnimatorListener) {
        view.visibility = View.VISIBLE
        ViewCompat.animate(view)
                .scaleX(1.0f)
                .scaleY(1.0f)
                .alpha(1.0f)
                .setDuration(200)
                .setListener(viewPropertyAnimatorListener)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .start()
    }

    // 隐藏view
    fun scaleHide(view: View, viewPropertyAnimatorListener: ViewPropertyAnimatorListener) {
        ViewCompat.animate(view)
                .scaleX(0.0f)
                .scaleY(0.0f)
                .alpha(0.0f)
                .setDuration(200)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .setListener(viewPropertyAnimatorListener)
                .start()
    }

    /**
     * 水平移动动画
     * @param view 执行动画的view
     * @param movePx 移动距离（单位px）
     */
    fun moveX(view: View, movePx: Int, duration: Long = 200, viewPropertyAnimatorListener: ViewPropertyAnimatorListener = object : ViewPropertyAnimatorListener {
        override fun onAnimationCancel(p0: View?) {
        }

        override fun onAnimationEnd(p0: View?) {
        }

        override fun onAnimationStart(p0: View?) {
        }
    }) {
        ViewCompat.animate(view)
            .translationX(movePx.toFloat())
            .setDuration(duration)
            .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
            .setListener(viewPropertyAnimatorListener)
            .start()
    }

    /**
     * 纵向移动动画
     * @param view
     * @param movePx
     */
    fun moveY(view: View, movePx: Int) {
        ViewCompat.animate(view)
                .translationY(movePx.toFloat())
                .setDuration(200)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .start()
    }

    /**
     * 向左移动动画
     * @param view 执行动画的view
     * @param movePx 移动距离（单位px）
     */
    fun moveToLeft(view: View, movePx: Int) {
        ViewCompat.animate(view)
                .translationX(movePx.toFloat())
                .setDuration(200)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .start()
    }

    fun moveYAndScaleShow(view: View, movePx: Int, viewPropertyAnimatorListener: ViewPropertyAnimatorListener) {
        ViewCompat.animate(view)
                .translationY(movePx.toFloat())
                .setDuration(200)
                .alpha(1.0f)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .setListener(viewPropertyAnimatorListener)
                .start()
    }

    fun moveYAndScaleHide(view: View, movePx: Int, viewPropertyAnimatorListener: ViewPropertyAnimatorListener) {
        ViewCompat.animate(view)
                .translationY(movePx.toFloat())
                .setDuration(200)
                .alpha(0.0f)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .setListener(viewPropertyAnimatorListener)
                .start()
    }
}
