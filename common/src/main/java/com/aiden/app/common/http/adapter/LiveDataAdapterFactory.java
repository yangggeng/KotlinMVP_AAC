package com.aiden.app.common.http.adapter;

import android.arch.lifecycle.LiveData;

import com.aiden.app.common.util.LogUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;

public final class LiveDataAdapterFactory extends CallAdapter.Factory {

    private static final String LOG_TAG = LiveDataAdapterFactory.class.getSimpleName();

    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        if (getRawType(returnType) != LiveData.class) {
            return null;
        }
        Type observableType = getParameterUpperBound(0, (ParameterizedType) returnType);
        LogUtils.INSTANCE.d(LOG_TAG, "observableType=" + observableType.toString());

        Class<?> rawObservableType = getRawType(observableType);
        LogUtils.INSTANCE.d(LOG_TAG, "rawObservableType=" + rawObservableType.getSimpleName());
//        if (rawObservableType != BaseResponse.class) {
//            throw new IllegalArgumentException("type must be BaseResponse");
//        }
//        if (!(observableType instanceof ParameterizedType)) {
//            throw new IllegalArgumentException("resource must be parameterized");
//        }
//        Type bodyType = getParameterUpperBound(0, (ParameterizedType) observableType);
//        LogUtils.INSTANCE.d(LOG_TAG, "bodyType=" + bodyType.toString());
        return new LiveDataCallAdapter<>(observableType);
    }
}
