package com.aiden.app.common.extension

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.widget.Toast

/**
 * Descriptions：自定义的一个 toast
 * <p>
 * Author：ChenME
 * Date：2019/3/5
 * Email：ibelieve1210@163.com
 */

private var toast: Toast? = null // Toast对象
private val handler = Handler(Looper.getMainLooper())

/**
 * 短 Toast
 */
fun Context.sToast(messageRes: Int) {
    this.mToast(this.getString(messageRes).toString())
}

fun Context.sToast(message: String) {
    this.mToast(message)
}

/**
 * 长 Toast
 */
fun Context.lToast(messageRes: Int) {
    this.mToast(this.getString(messageRes).toString(), true)
}

fun Context.lToast(message: String) {
    this.mToast(message, true)
}

fun Context.mToast(msg: String, isLongToast: Boolean = false) {
    Thread(Runnable {
        kotlin.run {
            handler.post {
                synchronized(this) {
                    if (toast != null) {
                        toast!!.cancel()
                        toast = null
                    }
                    toast = Toast.makeText(this, msg, if (isLongToast) Toast.LENGTH_LONG else Toast.LENGTH_SHORT)
                    toast!!.show()
                }
            }
        }
    }).start()
}
