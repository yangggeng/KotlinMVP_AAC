package com.aiden.app.common.constant

object Constants {
    const val REQUEST_CODE_CAMERA = 0x101
    const val REQUEST_CODE_ALBUM = 0x102
    const val REQUEST_CODE_CROP = 0x103

    const val JPUSH_EXTRA_BUNDLE = "extra_bundle" // 推送通知参数

    // 设备制造商
    const val HUAWEI = "huawei"
    const val XIAOMI = "xiaomi"
    const val MEIZU = "meizu"
    const val OTHER = "other"

    const val Prefix_IMUser = "dahupingtai" // 云信IM用户名前缀

    const val Param_RecentContactsFragment_IsSelectContact = "isSelectContact"
    const val Param_RecentContactsFragment_IMMessage = "iMMessage"

    const val DefaultAdCode = "130100" // 默认城市adCode，石家庄市
    const val DefaultCity = "石家庄市" // 默认城市
}