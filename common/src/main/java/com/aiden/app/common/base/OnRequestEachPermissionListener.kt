package com.aiden.app.common.base

interface OnRequestEachPermissionListener {

    fun onGranted(index: Int)

    fun onDenied(index: Int)
}