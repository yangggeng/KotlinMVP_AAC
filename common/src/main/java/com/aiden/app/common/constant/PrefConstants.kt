package com.aiden.app.common.constant

object PrefConstants {

    const val USER_ID = "user_id"
    const val StatusBarHeight = "statusBarHeight"
    const val Token = "token"
    const val PREF_HOST_INDEX = "CURRENT_HOST_INDEX"
    const val NotchHeight = "NotchHeight" // 刘海屏高度
}