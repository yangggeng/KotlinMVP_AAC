package com.aiden.app.common.extension

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/5/31
 * Email：ibelieve1210@163.com
 */

fun <T> Collection<T>?.isNil(): Boolean {
    if (null == this) {
        return true
    }
    return this.isEmpty()
}

fun <T> Collection<T>?.isNotNil(): Boolean = !isNil()

fun <T> List<T>?.notNullValue(): Collection<T> {
    return if (this.isNil()) emptyList() else this!!
}