package com.aiden.app.common.util

import kotlin.math.abs

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/3/7
 * Email：ibelieve1210@163.com
 */
object MoneyUtils {

    /**
     * 将分转换成元显示
     */
    fun fen2Yuan(fen: Long): String {
        var result = ""
        val tmp = abs(fen)
        result = when {
            tmp < 10 -> "0.0$tmp"
            tmp < 100 -> "0.$tmp"
            else -> {
                val tmpStr = tmp.toString()
                StringBuilder(tmpStr).insert(tmpStr.length - 2, ".").toString()
            }
        }
        return if (fen < 0) "-$result" else result
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println(fen2Yuan(-9))
        println(fen2Yuan(-90))
        println(fen2Yuan(-900))
        println(fen2Yuan(9))
        println(fen2Yuan(99))
        println(fen2Yuan(999))
        println(fen2Yuan(99999))
    }
}