package com.aiden.app.common.util

import android.content.Context
import android.content.SharedPreferences
import com.aiden.app.common.base.BaseApplication
import com.aiden.app.common.constant.ConfigConstants

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/3/19
 * Email：ibelieve1210@163.com
 */
object SPUtils {
    private var sp: SharedPreferences = BaseApplication.INSTANCE.getSharedPreferences(ConfigConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE)
    private var ed: SharedPreferences.Editor

    init {
        ed = sp.edit()
    }

    /**
     * Boolean数据
     */
    fun saveBoolean(key: String, value: Boolean) {
        ed.putBoolean(key, value)
        ed.commit()
    }

    /**
     * 默认 false
     */
    fun getBoolean(key: String): Boolean {
        return sp.getBoolean(key, false)
    }

    /**
     * String数据
     */
    fun saveString(key: String, value: String) {
        ed.putString(key, value)
        ed.commit()
    }

    /**
     * 默认 ""
     */
    fun getString(key: String): String {
        return sp.getString(key, "")
    }

    /**
     * Int数据
     */
    fun saveInt(key: String, value: Int) {
        ed.putInt(key, value)
        ed.commit()
    }

    /**
     * 默认 0
     */
    fun getInt(key: String): Int {
        return sp.getInt(key, 0)
    }

    /**
     *  Long数据
     */
    fun saveLong(key: String, value: Long) {
        ed.putLong(key, value)
        ed.commit()
    }

    /**
     *  默认 0
     */
    fun getLong(key: String): Long {
        return sp.getLong(key, 0)
    }

    /**
     * Set数据
     */
    fun saveStringSet(key: String, set: Set<String>) {
        val localSet = getStringSet(key).toMutableSet()
        localSet.addAll(set)
        ed.putStringSet(key, localSet)
        ed.commit()
    }

    /**
     * 默认空set
     */
    fun getStringSet(key: String): Set<String> {
        val set = setOf<String>()
        return sp.getStringSet(key, set)
    }

    /**
     * 删除key数据
     */
    fun remove(key: String) {
        ed.remove(key)
        ed.commit()
    }
}
