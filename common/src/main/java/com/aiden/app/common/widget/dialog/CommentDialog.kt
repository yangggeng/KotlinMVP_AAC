package com.aiden.app.common.widget.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import com.aiden.app.common.R
import com.aiden.app.common.extension.isNil

/**
 * 评论对话框（底部弹出，输入法顶起对话框）
 *
 * @author yanggeng
 * @date 2019/1/30 下午3:38
 */
class CommentDialog(context: Context) : Dialog(context, R.style.CommentDialog) {

    private lateinit var etComment: EditText
    private lateinit var tvSend: TextView
    private var onEditCompletedListener: OnEditCompletedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_comment)
        setCanceledOnTouchOutside(true)
        setCancelable(true)
        window?.setGravity(Gravity.BOTTOM)
        val lp = window?.attributes!!
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        window?.attributes = lp
        etComment = findViewById(R.id.et_comment)
        tvSend = findViewById(R.id.tv_send)

        etComment.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
//                tvSend.isEnabled = s?.toString()?.trim()?.isNotEmpty()!!
                tvSend.isEnabled = !s?.toString()?.trim().isNil()
            }
        })
        tvSend.setOnClickListener {
            if (onEditCompletedListener != null) {
                onEditCompletedListener?.onEditCompleted(etComment.text.toString())
                etComment.setText("")
            }
        }
    }

    fun setOnEditCompletedListener(onEditCompletedListener: OnEditCompletedListener) {
        this.onEditCompletedListener = onEditCompletedListener
        tvSend.setOnClickListener {
            onEditCompletedListener.onEditCompleted(etComment.text.toString())
            etComment.setText("")
        }
    }

    interface OnEditCompletedListener {
        fun onEditCompleted(text: String)
    }
}