package com.aiden.app.common.web

import java.util.regex.Matcher
import java.util.regex.Pattern

fun formatHtml(content: String): String {
    return "<html>\n" +
            "<head>\n" +
            "<meta name=\"viewport\" content=\"initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" /> \n" +
            "<style type=\"text/css\\\"> \n" +
            "body {font-size:28px; color:#FFFFFF;}\n" +
            "</style> \n" +
            "</head> \n" +
            "<body>\n" +
            "<script type='text/javascript'>\n" +
            "window.onload = function(){\n" +
            "var \$img = document.getElementsByTagName('img');\n" +
            "for(var p in  \$img){\n" +
            " \$img[p].style.width = '100%';\n" +
            "\$img[p].style.height ='auto'\n" +
            "}\n" +
            "}\n" +
            "</script>\n" +
            content +
            "</body>\n" +
            "</html>"
}

/**
 * 过滤获取富文本中图片列表
 */
fun filterImg(content: String): List<String> {
    val list = LinkedHashSet<String>()
    var img = ""
    val p_image: Pattern
    val m_image: Matcher
    // String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
    val regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>"
    p_image = Pattern.compile(regEx_img, Pattern.CASE_INSENSITIVE)
    m_image = p_image.matcher(content)
    while (m_image.find()) {
        // 得到<img />数据
        img = m_image.group()
        // 匹配<img>中的src数据
        val m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img)
        while (m.find()) {
            list.add(m.group(1).replace("'", ""))
        }
    }
    return list.toList()
}

fun formatProductDetail(content: String): String {
    return "<html>\n" +
            "<head>\n" +
            "<meta name=\"viewport\" content=\"initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" /> \n" +
            "<style type=\"text/css\\\"> \n" +
            "body {font-size:15px;}\n" +
            "c-b3 {color: #666666;}\n" +
            "fs-l {font-size: 5px;}\n" +
            "fs-m {font-size: 14px;}\n" +
            "</style> \n" +
            "</head> \n" +
            "<body>\n" +
            "<script type='text/javascript'>\n" +
            "window.onload = function(){\n" +
            "var \$img = document.getElementsByTagName('img');\n" +
            "for(var p in  \$img){\n" +
            " \$img[p].style.width = '100%';\n" +
            "\$img[p].style.height ='auto'\n" +
            "}\n" +
            "}\n" +
            "</script>\n" +
            content + "\n" +
            "<div id=\"remark\" class=\"fs-m c-b3\">\n" +
            "                <p>价格说明</p>\n" +
            "                <ul>\n" +
            "                    <li>\n" +
            "                        <p class=\"fs-l c-b3\">划线价格</p>\n" +
            "                        商品的专柜价、吊牌价、正品零售价、厂商指导价或该商品曾经展示过的销售价等，并非原价，仅供参考。\n" +
            "                    </li>\n" +
            "                    <li>\n" +
            "                        <p class=\"fs-l c-b3\">未划线价格</p>\n" +
            "                        商品的实时标价，不因表述的差异改变性质。具体成交价格根据商品参加活动，或会员使用优惠券、积分等发生变化，最终以订单结算页价格为准。\n" +
            "                    </li>\n" +
            "                    <li>商家详情页（含主图）以图片或文字形式标注的一口价、促销价、优惠价等价格可能是在使用优惠券、满减或特定优惠活动和时段等情形下的价格，具体请以结算页面的标价、优惠条件或活动规则为准。</li>\n" +
            "                    <li>若商家单独对划线价格进行说明的，以商家的表述为准。</li>\n" +
            "                </ul>\n" +
            "            </div>" +
            "</body>\n" +
            "</html>"
}