package com.aiden.app.common.util

import android.content.Context
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder

object AssetUtil {


    fun testJson(context: Context): String {
        return getJson(context, "province.json")
    }

    fun getJson(context: Context, fileName: String): String {
        val stringBuilder = StringBuilder()
        val assetManager = context.assets
        val bf = BufferedReader(InputStreamReader(assetManager.open(fileName)))
        var line: String ?= null
        line = bf.readLine()
        while (line != null) {
            stringBuilder.append(line)
            line = bf.readLine()
        }
        return stringBuilder.toString()
    }
}