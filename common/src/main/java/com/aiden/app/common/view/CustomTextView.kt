package com.aiden.app.common.view

import android.content.Context
import android.graphics.Paint
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.Gravity
import android.view.View

class CustomTextView : AppCompatTextView {

    private var fontMetricsInt = Paint.FontMetricsInt()
    private var paddingEnable = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    init {
        gravity = Gravity.CENTER_VERTICAL
        includeFontPadding = false
        val height = getViewHeight(this)

        val textSize = (getTextSize() + 1).toInt()
        var padding = (height - getTextSize()).toInt()
        if (height / 2 == 0 && textSize % 2 == 0) {
            setPadding(0, -padding, 0, -padding)
        } else {
            padding -= 1;
            setPadding(0, -padding, 0, -padding)
        }
    }

//    override fun onDraw(canvas: Canvas?) {
//        if (!paddingEnable) {
//            paint.getFontMetricsInt(fontMetricsInt)
//            canvas?.translate(0f, (fontMetricsInt.top - fontMetricsInt.ascent).toFloat())
//        }
//        super.onDraw(canvas)
//    }

    fun getViewHeight(view: View): Int {
        val width = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        val height = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(width, height);
        return view.getMeasuredHeight();
    }

}