package com.aiden.app.common.widget.dialog

import android.app.Activity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aiden.app.common.R
import com.aiden.app.common.base.BaseRecyclerViewAdapter
import com.aiden.app.common.extension.string
import com.aiden.app.common.entity.TabView
import com.lxj.xpopup.core.BottomPopupView

class BottomShareDialog(var activity: Activity, var onShareSelect: OnShareSelect) : BottomPopupView(activity) {

    private val otherTypeList = mutableListOf<TabView>()
    private lateinit var otherAdapter: BaseRecyclerViewAdapter<TabView>

    override fun getImplLayoutId(): Int {
        return R.layout.dialog_share
    }

    fun loadView(): BottomShareDialog {
        val rvShare = findViewById<RecyclerView>(R.id.rv_share)
        val rvOther = findViewById<RecyclerView>(R.id.rv_other)
        findViewById<Button>(R.id.btn_cancel).setOnClickListener { dismiss() }

        rvShare.layoutManager = GridLayoutManager(activity, 3)
        val shareTypeList = arrayOf(
                TabView("朋友圈", R.mipmap.ic_wechat_share_pyq),
                TabView("微信好友", R.mipmap.ic_wechat_share_friend),
                TabView("QQ好友", R.mipmap.ic_qq_share_friend),
                TabView("复制链接", R.mipmap.ic_share_copy)
        )
        val adapter = BaseRecyclerViewAdapter(context = activity, data = shareTypeList.toMutableList(), layoutId = R.layout.layout_item_tab) { v: View, tabView: TabView, position: Int ->
            v.findViewById<ImageView>(R.id.iv_icon).setImageResource(tabView.icon)
            v.findViewById<TextView>(R.id.tv_text).text = tabView.text
        }
        adapter.setOnItemClickListener(object : BaseRecyclerViewAdapter.OnItemClickListener {
            override fun onItemClick(v: View, position: Int) {
                onShareSelect.onClick(position)
                dismiss()
            }
        })
        rvShare.setHasFixedSize(true)
        rvShare.adapter = adapter

        otherTypeList.add(TabView("复制链接", R.mipmap.ic_share_copy))
        otherTypeList.add(TabView(context.string(R.string.string_popView_menuCollectedNo), R.mipmap.app_logo))
        otherTypeList.add(TabView("举报", R.mipmap.app_logo))
//        rvOther.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        otherAdapter = BaseRecyclerViewAdapter(activity, otherTypeList, R.layout.layout_item_tab) { v: View, tabView: TabView, position: Int ->
            v.findViewById<ImageView>(R.id.iv_icon).setImageResource(tabView.icon)
            v.findViewById<TextView>(R.id.tv_text).text = tabView.text
        }
//        otherAdapter.setOnItemClickListener(object : BaseRecyclerViewAdapter.OnItemClickListener {
//            override fun onItemClick(v: View, position: Int) {
//                when (position) {
//                    0 -> onShareSelect.onClick(BottomDialog.COPY)
//                    1 -> onShareSelect.onClick(BottomDialog.COLLECT)
//                    2 -> onShareSelect.onClick(BottomDialog.REPORT)
//                }
//                dismiss()
//            }
//        })
//        rvOther.setHasFixedSize(true)
//        rvOther.adapter = otherAdapter

        return this
    }



    interface OnShareSelect {
        fun onClick(selectId: Int)
    }

    fun updateCollectionStatus(isCollected: Boolean) {
        otherTypeList[1].text = context.string(if (isCollected) R.string.string_popView_menuCollectedYes else R.string.string_popView_menuCollectedNo)
        otherAdapter.notifyDataSetChanged()
    }

    companion object {

        private val TAG = "BottomSelectDialog"

        val SHARE_TO_TIMELINE = 0
        val SHARE_TO_WECHAT_FRIEND = 1
        val SHARE_TO_QQ_FRIEND = 2
        val COPY = 3
        val SHARE_TO_WEIBO = 4
        val SHARE_MORE = 5
        val COLLECT = 7
        val REPORT = 8
    }
}