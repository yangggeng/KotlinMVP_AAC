package com.aiden.app.common.constant

object ConfigConstants {

    const val SHARED_PREFERENCES = "dahu"
    const val WX_APP_ID = "wxd2a2e4ddcb6adbaf"
    const val QQ_APP_ID = "101518126"

    const val UMENG_APP_KEY = "5be2473fb465f5490e0002a0"

    const val COUNT_DOWN_MILLS = 60_000L

    const val SERVICE_PHONE = "4007003696"

    const val YUNXIN_APP_KEY = "191d10c4d47726c721fb20201067c2e4"

    const val HEADER_MAC_NO = "macNo" // mac地址
    const val HEADER_INTERFACE_VERSION = "interfaceVersion" // 接口版本号
    const val HEADER_MOBILE_MODEL = "mobileModel" // 手机型号
    const val HEADER_REQUEST_SOURCE = "requestSource" // 请求源
    const val HEADER_MOBILE_OS_VERSION = "mobileOsVersion" //手机系统版本
    const val HEADER_APP_VERSION = "appVersion" // App版本号
    const val HEADER_DEVICE_ID = "deviceId" // 设备id
    const val HEADER_TOKEN = "Authorization" // 登录返回token
    const val HEADER_TOKEN_PREFIX = "Bearer " // token前缀

    const val MIPUSH_APP_ID = "2882303761517873514"
    const val MIPUSH_APP_KEY = "5411787333514"

    const val MEIZU_APP_ID = "119380"
    const val MEIZU_APP_KEY = "f6b1882449fb4eba9db92ab89298bde7"

    const val NetEase_MIPush_Certificate = "DahuXM" // 云信小米推送证书名称
    const val NetEase_HWPush_Certificate = "DahuHW" // 云信华为推送证书名称
    const val NetEase_MZPush_Certificate = "DahuMZ" // 云信魅族推送证书名称
}