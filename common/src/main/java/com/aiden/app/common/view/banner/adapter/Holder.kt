package com.aiden.app.common.view.banner.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

open abstract class Holder<T> : RecyclerView.ViewHolder {
    constructor(itemView: View): super(itemView) {
        initView(itemView)
    }

    protected abstract fun initView(itemView: View)

    public abstract fun updateUI(data: T)
}