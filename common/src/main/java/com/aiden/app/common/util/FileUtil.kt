package com.aiden.app.common.util

import android.content.Context
import android.os.Environment
import com.aiden.app.common.base.BaseApplication
import com.aiden.app.common.event.EventMessage
import com.aiden.app.common.event.EventTag
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.runOnUiThread
import java.io.File
import java.text.DecimalFormat

object FileUtil {

    fun getHeadDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "dahuHead")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getStoreDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "dahuStore")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getRequirementDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "dahuRequirement")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getDownloadDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "dahuDownload")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getNimDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "nim")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getOrderDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "dahuOrder")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getPlanDir(context: Context): String {
        val file = File(context.externalCacheDir?.absolutePath + File.separator + "dahuPlan")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.path
    }

    fun getCacheSize(context: Context): String {
        var fileSize: Long = 0
        var cacheSize = "0KB"
        fileSize += getDirSize(context.cacheDir)
        fileSize += getDirSize(context.externalCacheDir!!)
        if (fileSize > 0)
            cacheSize = formatFileSize(fileSize)
        return cacheSize
    }

    fun deleteCache(context: Context) {
        deleteFile(context.cacheDir)
        deleteFile(context.externalCacheDir!!)

        context.runOnUiThread {
            EventBus.getDefault().post(EventMessage(EventTag.CLEAR_CACHE))
        }
    }

    fun deleteFile(file: File) {
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            if (file.exists()) {
                if (file.isFile) {
                    file.delete()
                } else if (file.isDirectory) { // 如果它是一个目录
                    // 声明目录下所有的文件 files[];
                    val files = file.listFiles()
                    if (files != null && files.size > 0) {
                        for (i in files.indices) { // 遍历目录下所有的文件
                            deleteFile(files[i]) // 把每个文件 用这个方法进行迭代
                        }
                    }
                }
                file.delete()
            }
        }
    }

    fun getDirSize(dir: File): Long {
        if (!dir.isDirectory) {
            return 0
        }
        var dirSize: Long = 0
        val files = dir.listFiles()
        if (files != null && files.size > 0) {
            for (file in files) {
                if (file.isFile) {
                    dirSize += file.length()
                } else if (file.isDirectory) {
                    dirSize += file.length()
                    dirSize += getDirSize(file) // 递归调用继续统计
                }
            }
        }
        return dirSize
    }

    fun formatFileSize(fileSize: Long): String {
        var df = DecimalFormat("#.00")
        var fileSizeString = ""
        if (fileSize < 1024) {
            fileSizeString = df.format(fileSize.toDouble()) + "B"
        } else if (fileSize < 1048576) {
            fileSizeString = df.format(fileSize.toDouble() / 1024) + "K"
        } else if (fileSize < 1073741824) {
            fileSizeString = df.format(fileSize.toDouble() / 1048576) + "M"
        } else {
            fileSizeString = df.format(fileSize.toDouble() / 1073741824) + "G"
        }
        return fileSizeString
    }

    fun getImageFileName(): String {
        return "${BaseApplication.INSTANCE.userId}${System.currentTimeMillis()}.png"
    }
}