package com.aiden.app.common.web

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.aiden.app.common.R
import com.aiden.app.common.base.BaseActivity
import com.aiden.app.common.base.BaseApplication
import com.aiden.app.common.extension.TAG
import com.aiden.app.common.extension.isNil
import com.aiden.app.common.extension.isNotNil
import com.aiden.app.common.util.LogUtils
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.smtt.sdk.WebChromeClient
import com.tencent.smtt.sdk.WebSettings
import com.tencent.smtt.sdk.WebView
import com.tencent.smtt.sdk.WebViewClient
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_webview.*

/**
 * 公共webview界面
 *
 * @author yanggeng
 * @date 2019/4/16 11:37
 */
open class BrowserActivity : BaseActivity() {

    companion object {
        const val URL = "URL" // 链接
        const val DATA = "data" // 富文本
        const val TITLE = "title" // 标题
        const val SHOW_LEFT = "showLeft" // 是否显示返回按钮
        const val RIGHT_TEXT = "rightText"
        const val RIGHT_ICON = "rightIcon"
    }

    private lateinit var wxapi: IWXAPI
    private var title: String ?= ""
    private var url: String ?= ""
    private var data: String ?= ""

    protected lateinit var mWebView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_webview)
        intent?.apply {
            url = this.getStringExtra(URL)
            this@BrowserActivity.data = getStringExtra(DATA)
            title = getStringExtra(TITLE)
            val showLeft = getBooleanExtra(SHOW_LEFT, true)
            val rightIcon = getIntExtra(RIGHT_ICON, -1)
            setTitle(showLeft, title, rightText = getStringExtra(RIGHT_TEXT), rightBtnRes = rightIcon)
        }
        setDarkStatusBar()

        mWebView = findViewById(R.id.webview)

        webview.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, progress: Int) {
                progress_bar.progress = progress
                if (progress == 100) {
                    progress_bar.visibility = View.GONE
                } else {
                    progress_bar.visibility = View.VISIBLE
                }
            }

            @SuppressLint("SetTextI18n")
            override fun onReceivedTitle(view: WebView?, title: String?) {
                if (title.isNil() || this@BrowserActivity.title.isNotNil()) {
                    return
                }
                if (title?.length!! > MAX_TITLE_LENGTH) {
                    tv_title.text = "${title.substring(0, MAX_TITLE_LENGTH)}..."
                } else {
                    tv_title.text = title
                }
            }
        }

        webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(webview: WebView?, url: String): Boolean {
                if (!url.startsWith("http")) {
//                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
//                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
//                    startActivity(intent)
                    return false
                }
                webview?.loadUrl(url)
                return super.shouldOverrideUrlLoading(webview, url)
            }
        }

        val webSettings = webview.settings
        webSettings.javaScriptEnabled = true
        webSettings.builtInZoomControls = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.domStorageEnabled = true
        webSettings.allowFileAccess = true
        webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS
        webSettings.useWideViewPort = true
        webSettings.setAppCacheEnabled(true)
        webSettings.setGeolocationEnabled(true)
        webSettings.setAppCacheMaxSize(Long.MAX_VALUE)
        webSettings.pluginState = WebSettings.PluginState.ON_DEMAND
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH)

//        webview.x5WebViewExtension.setScrollBarFadingEnabled(false)

        if (url.isNotNil()) {
            webview.loadUrl(url)
        } else {
            if (data.isNotNil()) {
                webview.loadDataWithBaseURL(null, this.data, null, "utf-8", null)
            }
        }

        LogUtils.e(TAG, "webview X5Extension ${webview.x5WebViewExtension}")

//        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE or WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        wxapi = BaseApplication.INSTANCE.getWXApi()
    }

    override fun loadData() {
    }

    override fun onRightButtonClicked() {

    }
}