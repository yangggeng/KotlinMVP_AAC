package com.aiden.app.common.widget.dialog

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.widget.ImageView
import com.lxj.xpopup.core.BasePopupView
import com.lxj.xpopup.core.CenterPopupView
import com.aiden.app.common.R

/**
 * 加载框
 *
 * @author yanggeng
 * @date 2019/7/15 10:22
 */
class LoadingDialog(context: Context) : CenterPopupView(context) {

    private var animationDrawable: AnimationDrawable? = null
    private lateinit var ivLoading: ImageView

    override fun getImplLayoutId(): Int {
        return R.layout.dialog_loading
    }

    override fun onCreate() {
        super.onCreate()
        ivLoading = findViewById(R.id.iv_loading)
    }

    override fun show(): BasePopupView {
        if (animationDrawable == null) {
            findViewById<ImageView>(R.id.iv_loading).setImageResource(R.drawable.comm_refresh)
            animationDrawable = findViewById<ImageView>(R.id.iv_loading).drawable as AnimationDrawable
        }
        animationDrawable?.start()
        return super.show()
    }

    override fun dismiss() {
        super.dismiss()
        animationDrawable?.stop()
    }
}