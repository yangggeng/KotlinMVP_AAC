package com.aiden.app.common.util

import android.os.Build
import java.net.NetworkInterface
import java.util.*


object DeviceUtil {

    private const val TAG = "DeviceInfo"

    fun getMac(): String {
        try {
            val all = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (!nif.name.equals("wlan0", true)) continue

                val macBytes = nif.hardwareAddress ?: return ""

                val res1 = StringBuilder()
                for (b in macBytes) {
                    res1.append(String.format("%02X:", b))
                }

                if (res1.isNotEmpty()) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return "02:00:00:00:00:00"
    }

    fun getModel(): String {
        var mfg = "unknown"
        try {
            val buildClass = Build::class.java
            mfg = buildClass.getField("MODEL").get(null) as String
        } catch (e: Exception) {
            LogUtils.e(TAG, e.message!!)
        }
        return mfg
    }

    fun getOSVersion(): String {
        return Build.VERSION.SDK_INT.toString()
    }

    fun getManufacturer(): String {
        var mfg = "unknown" //$NON-NLS-1$
        try {
            val buildClass = Build::class.java
            mfg = buildClass.getField("MANUFACTURER").get(null) as String //$NON-NLS-1$
        } catch (ignore: Exception) {
            LogUtils.d(TAG, "Caught exception", ignore) //$NON-NLS-1$
        }
        return mfg
    }
}