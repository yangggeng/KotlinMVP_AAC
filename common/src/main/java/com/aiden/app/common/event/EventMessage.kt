package com.aiden.app.common.event

import java.io.Serializable

data class EventMessage(var tag: String, var param: Any? = null, var extra: Any? = null, var other: Any? = null) : Serializable