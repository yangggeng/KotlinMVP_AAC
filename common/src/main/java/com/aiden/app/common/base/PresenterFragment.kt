package com.aiden.app.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class PresenterFragment<P : BaseFragmentPresenter<*>>(var mPresenter: P) : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        lifecycle.addObserver(mPresenter)
        view()

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    abstract fun view()
}