package com.aiden.app.common.view

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 *  禁止左右滑动的viewpager
 *
 * @author yanggeng
 * @date 2019/1/17 上午9:22
 */
class NoScrollViewPager : ViewPager {

    var noScrollable = true

    constructor(context: Context): super(context)

    constructor(context: Context, attrs: AttributeSet): super(context, attrs)

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if (noScrollable) {
            return false
        } else {
            return super.onTouchEvent(ev)
        }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (noScrollable) {
            return false
        } else {
            return super.onInterceptTouchEvent(ev)
        }
    }
}