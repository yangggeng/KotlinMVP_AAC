package com.aiden.app.common.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2019/3/11
 * Email：ibelieve1210@163.com
 */
abstract class BaseRecyclerViewAdapter2 <T, VH : RecyclerView.ViewHolder>(var mContext: Context) : RecyclerView.Adapter<VH>() {

    var mItemClickListener: OnItemClickListener<T>? = null // item 的点击事件
    var dataList: MutableList<T> = mutableListOf() // 数据集合

    /**
     * 设置数据
     */
    fun setData(sources: MutableList<T>) {
        dataList = sources
        notifyDataSetChanged()
    }

    /**
     * 追加数据
     */
    fun addData(sources: MutableList<T>) {
        dataList.addAll(sources)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.setOnClickListener {
            mItemClickListener?.onItemClick(dataList[position], position)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    /**
     * item 的点击事件
     */
    fun setOnItemClickListener(listener: OnItemClickListener<T>) {
        this.mItemClickListener = listener
    }

    /**
     * item 点击事件接口
     */
    interface OnItemClickListener<in T> {
        fun onItemClick(item: T, position: Int)
    }
}