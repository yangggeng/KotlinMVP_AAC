package com.aiden.app.common.extension

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.Context
import android.graphics.Point
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.provider.Settings
import android.support.annotation.ColorRes
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.umeng.analytics.MobclickAgent

val Context.screenWidth
    get() = resources.displayMetrics.widthPixels

val Context.screenHeight
    get() = resources.displayMetrics.heightPixels

val Context.TAG: String
    get() = this.javaClass.simpleName

fun Context.dp2px(value: Int): Int = (value * resources.displayMetrics.density).toInt()

fun Context.sp2px(value: Int): Int = (value * resources.displayMetrics.scaledDensity).toInt()

fun Context.px2dp(value: Int): Float = value.toFloat() / resources.displayMetrics.density

fun Context.px2sp(value: Int): Float = value.toFloat() / resources.displayMetrics.scaledDensity

fun Context.string(@StringRes resId: Int): String = getString(resId)

fun Context.color(@ColorRes resId: Int): Int = resources.getColor(resId)

fun Context.inflate(@LayoutRes resId: Int, parent: ViewGroup? = null, attachToRoot: Boolean = false): View = LayoutInflater.from(this).inflate(resId, parent, attachToRoot)

/**
 * App版本名
 * @return
 */
fun Context.getAppVersion(): String {
    val info = applicationContext.packageManager.getPackageInfo(applicationContext.packageName, 0)
    if (info != null) {
        return info.versionName
    }
    return ""
}

@SuppressLint("HardwareIds")
fun Context.getAndroidId(): String = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

fun Context.toast(text: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, duration).show()
}

fun Context.toast(@StringRes stringResId: Int, duration: Int = Toast.LENGTH_SHORT) {
    toast(string(stringResId), duration)
}

fun Context.density() = resources.displayMetrics.density

fun Context.getDrawableId(paramString: String): Int = resources.getIdentifier(paramString, "mipmap", packageName)

/**
 * 网络是否可用
 * @return
 */
fun Context.isNetWork(): Boolean {
    val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
    if (networkInfo != null) {
        return networkInfo.isConnected
    }
    return false
}

/**
 * wifi是否可用
 * @return
 */
fun Context.isWifiConnected(): Boolean {
    val networkInfo: NetworkInfo? = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
    if (networkInfo != null) {
        return networkInfo.isConnected
    }
    return false
}

fun Context.copyToClipBoard(text: String) {
    val clipData = ClipData.newPlainText(null, text)
    clipboardManager.primaryClip = clipData
}

fun Context.getHeight(): Int {
    val point = Point()
    windowService.defaultDisplay.getRealSize(point)
    return point.y
}

val Fragment.TAG: String
    get() = this.javaClass.simpleName

fun Context.onUmengEvent(eventId: String) {
    MobclickAgent.onEvent(this, eventId)
}