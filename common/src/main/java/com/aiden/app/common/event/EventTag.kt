package com.aiden.app.common.event

object EventTag {

    const val EVENT_LOGIN = "login"

    const val EVENT_NET_EXCEPTION = "net_exception"

    const val EVENT_GET_CODE = "get_code"

    const val BANK_ADD_SUCCESS = "bank_add_success"

    const val CLEAR_CACHE = "clear_cache"

    const val SELECT_CITY = "select_city"

    const val STATION_SUBMIT_SUCCESS = "station_submit_success"

    const val QQ_LOGIN_SUCCESS = "qq_login_success"

    const val QQ_LOGIN_CANCEL = "qq_login_cancel"

    const val QQ_LOGIN_ERROR = "qq_login_error"

    const val MAINTAIN_EDIT_GOODS = "maintain_edit_goods"

    const val WX_LOGIN = "wx_login"

    const val LOGOUT = "logout"

    const val SKU_SELECT = "sku_select"

    const val SKU_UNSELECT = "sku_unselect"

    const val Sku_Confirm = "sku_confirm" // 商品规格确认选择

    const val CAR_MODEL_SELECT = "car_model_select"

    const val SPECIFICATION_SUBMIT = "specification_submit" //规格提交

    const val SERVICE_ADDRESS = "service_address" // 服务地址更新

    const val STORE_BANNER = "store_banner" //店铺轮播图更新

    const val GOODS_DETAIL_DESC = "goods_detail_desc" // 商品详情描述信息

    const val Location_Completed = "location_completed" // 定位完成通知

    const val Car_Update = "car_update" // 车辆信息更新

    const val UserInfoUpdate = "user_info_update" // 用户信息更新
    const val InitUserInfo = "init_user_info" // 初始化用户信息
    const val Event_RefreshUserInfo = "event_refresh_user_info" // 刷新用户信息

    const val Address_Update = "address_update" // 用户收货地址更新

    const val StoreCoupon_Update = "store_coupon_update" // 商家优惠券更新

    const val OrderStatusChange_Cancel = "order_status_change_cancel" // 订单取消
    const val OrderStatusChange_Delete = "order_status_change_delete" // 订单删除
    const val OrderStatusChange_Confirm = "order_status_change_confirm" // 订单确认
    const val OrderStatusChange_CancelBookOrder = "order_status_change_cancel_book_order" // 预约订单取消

    const val Cart_Submit = "cart_submit" // 购物车提交订单
    const val Cart_CountUpdate = "cart_count_update" // 购物车数量发生变化

    const val StoreBrand_Update = "store_brand_update" // 店铺品牌更新

    const val StoreCategory_Update = "store_category_update" // 店铺分类更新

    const val WXPay_Success = "wechat_pay_success" // 微信支付成功
    const val WXPay_Failed = "wechat_pay_failed" // 微信支付失败
    const val Order_PaySuccess = "order_pay_success" // 订单付款成功

    const val Requirement_Finished = "requirement_finished" // 需求完成
    const val Requirement_Canceled = "requirement_canceled" // 需求撤销
    const val Requirement_Delete = "requirement_delete" // 需求删除
    const val Requirement_Respond = "requirement_respond" // 需求响应
    const val SellerInfo_Update = "sellerInfo_update" // 商家信息获取成功

    const val Demand_SavePublishSuccess = "Demand_SavePublishSuccess" // 需求 保存/发布 成功

    const val PlanAddGoods_GoodsCategory = "goods_category_selected" // 方案添加商品，商品分类选中
    const val PlanAddGoods_Search = "plan_add_goods_search" // 方案添加商品输入搜索

    const val InvoiceUpdate = "invoice_update" // 发票更新

    const val PlanListRefresh = "plan_list_refresh" // 更新方案列表
    const val PlanSaveSuccess = "plan_save_success" // 方案保存或推荐
    const val PlanSubmitSuccess = "plan_submit_success" // 方案生成预约单成功

    const val Event_OrderStore_CompleteService = "Event_OrderStore_CompleteService" // 商家订单 服务完成 成功
    const val Event_OrderStore_SendGoods = "Event_OrderStore_SendGoods" // 商家订单 发货成功 成功
    //    const val Event_OrderStore_Warn2Pay = "Event_OrderStore_Warn2Pay" // 商家订单 提醒支付 成功
    const val Event_OrderStore_ConfirmOrder = "Event_OrderStore_ConfirmOrder" // 商家订单 确认订单 成功
    const val Event_OrderStore_RefuseOrder = "Event_OrderStore_RefuseOrder" // 商家订单 拒绝接单 成功

    const val Event_SubmitSettle = "event_submit_settle" // 发起结算成功
    const val Event_SettleComplete = "event_settle_complete" // 结算完成
    const val Event_CloseSettleDetail = "event_close_detail" // 关闭结算详情
    const val Event_CreateSettleBill = "event_settle_bill" // 创建结算账单
    const val Event_SettleRemove = "event_settle_remove" // 结算订单移除

    const val Event_ApplyAfterSaleSuccess = "apply_after_sale_success" // 申请售后
    const val Event_AfterSaleSendGoods = "after_sale_send_goods" // 售后 退货/换货 用户发出原件
    const val Event_AfterSaleStatusChange = "after_sale_status_change" // 售后状态变更

    const val Event_PublishEvaluateSuccess = "Event_PublishEvaluateSuccess" // 发布评价成功

    /**
     * IM 聊天获取 seller Id
     */
    const val Event_IMChat_GetSellerInfo = "Event_IMChat_GetSellerInfo" // 获取用户的 Seller 信息
    const val Event_IMChat_TakeSellerInfo = "Event_IMChat_TakeSellerInfo" // 取出用户的 Seller 信息
    const val Event_IMChat_StartSellerStore = "Event_IMChat_StartSellerStore" // 跳转到 seller 的店铺

    const val Event_MainMessageCountChange = "Event_MainMessageCountChange" // 首页未读消息数更新，传值为总数
    const val Event_IMMessageCountUpdate = "Event_MainMessageCountUpdate" // IM未读消息数更新

    /**
     * 商品管理：
     * 提交审核：所有、待上架
     * 删除：所有、待上架、已删除
     * 上架：所有、待上架、在售、已下架
     * 下架：所有、在售、已下架
     */
    const val Event_GoodsManage_ReviewSuccess = "Event_GoodsManage_ReviewSuccess" // 提交审核：所有、待上架、已下架
    const val Event_GoodsManage_DelSuccess = "Event_GoodsManage_DelSuccess" // 删除：所有、待上架、已删除
    const val Event_GoodsManage_2ArriveSuccess = "Event_GoodsManage_2ArriveSuccess" // 上架：所有、待上架、在售、已下架
    const val Event_GoodsManage_2SoldOutSuccess = "Event_GoodsManage_2SoldOutSuccess" // 下架：所有、在售、已下架

    const val Event_RepairOrTechFocusChanged = "Event_RepairOrTechFocusChanged" // 维修店/技师关注数量发生变化

    const val Event_IMForwardMessage = "Event_IMForwardMessage" // IM转发消息到当前聊天对象

    const val Event_CarModelsSelected = "Event_CarModelsSelected" // 选择车型多选
}