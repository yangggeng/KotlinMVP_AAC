package com.aiden.app.common.util

import android.content.Context
import android.widget.FrameLayout
import com.aiden.app.common.R
import com.aiden.app.common.extension.color
import com.aiden.app.common.widget.time.CustomTimePickerBuilder
import com.aiden.app.common.widget.time.OnTimeSelectListener
import java.util.*

object TimeDialogUtils {

    fun showYMPicker(context: Context, onTimeSelectListener: OnTimeSelectListener) {
        val pvTime = CustomTimePickerBuilder(context, onTimeSelectListener)
                .setType(arrayOf(true, true, false, false, false, false).toBooleanArray())
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(true)
                .isDialog(false)
                .build()
        if (SystemUtil.isNavigationBarVisible(context)) {
            val layoutParams = pvTime.dialogContainerLayout.layoutParams as FrameLayout.LayoutParams
            layoutParams.bottomMargin = SystemUtil.getNavigationHeight(context)
            pvTime.dialogContainerLayout.layoutParams = layoutParams
        }
        pvTime.show()
    }

    fun showYMDPicker(context: Context, onTimeSelectListener: OnTimeSelectListener, startDate: Calendar? = null, endDate: Calendar? = null, title: String = "") {
        if (startDate != null && endDate != null) {
            if (startDate.timeInMillis > endDate.timeInMillis) {
                startDate.timeInMillis = endDate.timeInMillis
            }
        }
        val pvTime = CustomTimePickerBuilder(context, onTimeSelectListener)
                .setType(arrayOf(true, true, true, false, false, false).toBooleanArray())
                .setLabel("", "", "", "", "", "")
                .setRangDate(startDate, endDate)
                .setTitleText(title)
                .setCancelColor(context.color(R.color.color_gray_33))
                .isCenterLabel(true)
                .isDialog(false)
                .build()
        if (SystemUtil.isNavigationBarVisible(context)) {
            val layoutParams = pvTime.dialogContainerLayout.layoutParams as FrameLayout.LayoutParams
            layoutParams.bottomMargin = SystemUtil.getNavigationHeight(context)
            pvTime.dialogContainerLayout.layoutParams = layoutParams
        }
        pvTime.show()
    }

    fun showYMDHMSPicker(context: Context, onTimeSelectListener: OnTimeSelectListener, startDate: Calendar? = null, endDate: Calendar? = null, title: String = "") {
        if (startDate != null && endDate != null) {
            if (startDate.timeInMillis > endDate.timeInMillis) {
                startDate.timeInMillis = endDate.timeInMillis
            }
        }
        val pvTime = CustomTimePickerBuilder(context, onTimeSelectListener)
                .setType(arrayOf(true, true, true, true, true, true).toBooleanArray())
                .setLabel("", "", "", "", "", "")
                .setRangDate(startDate, endDate)
                .setTitleText(title)
                .setCancelColor(context.color(R.color.color_gray_33))
                .isCenterLabel(true)
                .isDialog(false)
                .build()
        if (SystemUtil.isNavigationBarVisible(context)) {
            val layoutParams = pvTime.dialogContainerLayout.layoutParams as FrameLayout.LayoutParams
            layoutParams.bottomMargin = SystemUtil.getNavigationHeight(context)
            pvTime.dialogContainerLayout.layoutParams = layoutParams
        }
        pvTime.show()
    }

    fun showYMDExtrasPicker(context: Context, onTimeSelectListener: OnTimeSelectListener, extras: MutableList<String>, startDate: Calendar? = null, endDate: Calendar? = null) {
        if (startDate != null && endDate != null) {
            if (startDate.timeInMillis > endDate.timeInMillis) {
                startDate.timeInMillis = endDate.timeInMillis
            }
        }
        val pvTime = CustomTimePickerBuilder(context, onTimeSelectListener)
                .setType(arrayOf(true, true, true, false, false, false).toBooleanArray())
                .setLabel("", "", "", "", "", "")
                .setRangDate(startDate, endDate)
                .setCancelColor(context.color(R.color.color_gray_33))
                .setSubmitColor(context.color(R.color.color_blue_2dbefe))
                .isCenterLabel(true)
                .isDialog(false)
                .setExtras(extras)
                .build()
        if (SystemUtil.isNavigationBarVisible(context)) {
            val layoutParams = pvTime.dialogContainerLayout.layoutParams as FrameLayout.LayoutParams
            layoutParams.bottomMargin = SystemUtil.getNavigationHeight(context)
            pvTime.dialogContainerLayout.layoutParams = layoutParams
        }
        pvTime.show()
    }
}